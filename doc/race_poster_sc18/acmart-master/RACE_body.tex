\section{Motivation}

Many sparse linear algebra kernels, such as symmetric sparse
matrix-vector multiplication (\SymmSpmv) or the Gauss-Seidel iteration, are hard
to parallelize due to write-after-write or read-after-write
dependencies.  In this poster we concentrate on \DTWO dependencies
like in \SymmSpmv. 

\section{Related Work}

Many solutions to the \DTWO problem have been proposed, such as
locking methods, thread-private target arrays
\cite{thread_private_symm_spmv}, special storage formats
\cite{CSB,RSB}, and matrix reordering, on which we focus
here. Multicoloring (MC) \cite{MC,COLPACK} and Algebraic Block
Multicoloring (ABMC) \cite{ABMC} are two widely used solutions in this
area. In \cite{feast_mc}, MC was applied to the CARP-CG
algorithm. However, reordering can impact data access locality,
increase the need for synchronization, and effect false sharing,
leading to low performance. Here we extend ABMC for \DTWO kernels in
order to mitigate these effects.


\section{Recursive Algebraic Coloring (RAC) method}

The method aims at improving data locality, reduce synchronization, and
generate sufficient parallelism while still retaining simple sparse
data formats like \CRSfull (\CRS).

RAC is a sequential, recursive, level-based algorithm that is applicable to
general \DK dependencies. It is currently limited to matrices
with symmetric structure (undirected graph), but possibly nonsymmetric
entries. In the following we describe the four steps of the
algorithm, which operate on the matrix graph.
\begin{enumerate}
\item \emph{Level construction}.  A breadth-first search (BFS)
  \cite{BFS} is done on the graph to improve data locality
  \cite{RCM_Sparse_computation}. In our experiments we substituted
  this stage with the slightly more complicated Reverse Cuthill-McKee
  algorithm (RCM) \cite{RCM}.
		
\item \emph{Permutation}. The matrix is permuted in the order of
  levels.  We additionally store an array containing the index
  of to the first element in each level (\levelPtr).
		
\item \emph{Distance-k coloring}. Using the levels $L(i)$ one can
  show that $L(i)$ and $L(i+(k+j))$ are \DK independent
  for all $j\geq1$. In case of $k=2$ this would mean if we
  leave at least a gap of two levels between any two groups of levels
  ($T(a)$ and $T(b)$) they are \DTWO independent. $T()$ is called \levelGroups and are formed by aggregating nearby levels $L()$.  One possible
  configuration can be seen in the figure for the case of two colors;
  each red \levelGroup is separated by at least two
  levels of blue and vice-versa. Obviously there is now
  a significant load imbalance because of the differently sized
  level groups.
  		
\item \emph{Load balancing}. The main idea is to resolve the \DK
  dependency as required by the algorithm at hand, but also distribute
  nonzeros evenly across the desired number of parallel threads.  This
  is done by assigning more levels in areas where the levels are
  small, but fewer levels where they are large, observing the minimum
  requirement of two levels for maintaining the \DTWO dependency.
  The algorithm tries to reduce the variance in the number of nonzeros
  in the two colors by acquiring or giving levels from or to the
  corresponding \levelGroup.
\end{enumerate}
If above steps do not lead to sufficient parallelism, recursion is
applied. A \subgraph is chosen  (typically a \levelGroup) based
on a global load balancing algorithm, which decides that splitting
a \levelGroup into multiple subgroups will be beneficial. Then
the four steps above are applied on this \subgraph. The thread
that was assigned to the parent \subgraph must spawn two or more
subthreads to work on the parts. 

\section{RACE library}

We have implemented the \RAC method in a library, the \RACEfull (\RACE).
Using \RACE implies a pre-processing and a processing phase.
In pre-processing the user supplies the matrix, the kernel requirements
(e.g., \DONE or \DTWO) and hardware settings (number of threads,
affinity strategy). The library generates a permutation and stores the
recursive coloring information in a \levelTree. It also creates
a pool of pinned threads to be used later. In the processing phase,
the user provides a sequential kernel function, which the library
executes in parallel as a callback, using the thread pool. 

\section{Performance}

The test setup for the performance measurements is available in the
artifacts description. Since in normal applications pre-processing is done
only once during matrix creation phase we compare only pure processing time.
We show the performance of \SymmSpmv % and \KACZ
for a range of matrices on Intel Ivy Bridge EP and Skylake SP,
comparing \RACE against \ABMC, \MC, and \MKL implementations. \RACE is
faster than the alternatives (by up to 2.5$\times$) for almost all
matrices, followed by \ABMC, \MKL, and \MC. The advantage of \RACE is
especially pronounced with large matrices, where data traffic and
locality of access is pivotal. For one matrix (nlpkkt-200) we show
performance of \SymmSpmv together with Roof\/line limits and data
traffic measurements (via \LIKWID \cite{LIKWID}).


With iterative solvers (like Kaczmarz), matrix reordering causes a change
in convergence behavior. The Kiviat graph shows the change in
iterations to convergence relative to the serial version (lower is
better). Here \RACE is on par with
\ABMC, followed by \MC. Hence, we show for Kaczmarz the GFlop/s rate as
well as the actual inverse time to solution.
