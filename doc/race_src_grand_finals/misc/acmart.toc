\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}User's guide}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Installation}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Invocation and options}{6}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Top matter}{7}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Algorithms}{19}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Figures and tables}{19}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Theorems}{21}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Online-only and offline-only material}{21}{subsection.2.7}
\contentsline {subsection}{\numberline {2.8}Note about anonymous mode}{21}{subsection.2.8}
\contentsline {subsection}{\numberline {2.9}Acknowledgments}{22}{subsection.2.9}
\contentsline {subsection}{\numberline {2.10}Bibliography}{22}{subsection.2.10}
\contentsline {subsection}{\numberline {2.11}Colors}{25}{subsection.2.11}
\contentsline {subsection}{\numberline {2.12}Other notable packages and typographic remarks}{26}{subsection.2.12}
\contentsline {subsection}{\numberline {2.13}A note for wizards: \texttt {acmart-preload-hook.tex}}{26}{subsection.2.13}
\contentsline {subsection}{\numberline {2.14}Currently supported publications}{27}{subsection.2.14}
\contentsline {section}{\numberline {3}Implementation}{30}{section.3}
\contentsline {subsection}{\numberline {3.1}Identification}{30}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Preload hook}{30}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Options}{30}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Setting switches}{33}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Loading the base class and package}{34}{subsection.3.5}
\contentsline {subsection}{\numberline {3.6}Citations}{35}{subsection.3.6}
\contentsline {subsection}{\numberline {3.7}Paper size and paragraphing}{42}{subsection.3.7}
\contentsline {subsection}{\numberline {3.8}Fonts}{45}{subsection.3.8}
\contentsline {subsection}{\numberline {3.9}Floats}{46}{subsection.3.9}
\contentsline {subsection}{\numberline {3.10}Lists}{48}{subsection.3.10}
\contentsline {subsection}{\numberline {3.11}Top-matter data}{49}{subsection.3.11}
\contentsline {subsection}{\numberline {3.12}Concepts system}{63}{subsection.3.12}
\contentsline {subsection}{\numberline {3.13}Copyright system}{64}{subsection.3.13}
\contentsline {subsection}{\numberline {3.14}Typesetting top matter}{69}{subsection.3.14}
\contentsline {subsection}{\numberline {3.15}Headers and Footers}{83}{subsection.3.15}
\contentsline {subsection}{\numberline {3.16}Sectioning}{89}{subsection.3.16}
\contentsline {subsection}{\numberline {3.17}TOC lists}{91}{subsection.3.17}
\contentsline {subsection}{\numberline {3.18}Theorems}{92}{subsection.3.18}
\contentsline {subsection}{\numberline {3.19}Acknowledgments}{95}{subsection.3.19}
\contentsline {subsection}{\numberline {3.20}Conditional typesetting}{95}{subsection.3.20}
\contentsline {subsection}{\numberline {3.21}Additional bibliography commands}{96}{subsection.3.21}
\contentsline {subsection}{\numberline {3.22}End of Class}{96}{subsection.3.22}
