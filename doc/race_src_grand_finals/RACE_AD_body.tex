\section{Abstract}
This article describes the experiment settings and how the performance runs were conducted for the results shown in the SC18 poster "Recursive Algebriac Coloring Engine". It gives a detailed information on how to get, compile and perform the runs using RACE library. Furthermore it also gives a brief description on the softwares and method used to perform the comparison runs.

\section{Description}
	\subsection{Check-list}
	\begin{itemize}
		\item \textbf{Algorithm}: RAC method explained in poster and extended summary.
		\item \textbf{Program:} C++,C program and libraries
		\item \textbf{Compilation:} Intel compiler v 17.0.3 with -O3 -xHOST
		\item \textbf{Hardware:} SkyLake (Gold 6148) @ 2.4 GHz and Ivy Bridge E5-2660 v2 @ 2.2 GHz
		\item \textbf{Run-time environment:} Ubuntu 16.04.5 LTS on SkyLake and CentOS Linux release 7.5.1804 on Ivy Bridge.
		\item \textbf{Data set:} Publicly available matrices.
		\item \textbf{Output:} performance in GFlop/s and iterations.
		\item \textbf{Experiment workflow:} Clone and install the RACE-AD repository, build, compile, run. 
		\item \textbf{Publicly available:} Pre-compiled version of the RACE library is publicly available.
	\end{itemize}
	
	\subsection{How software can be obtained}
	Clone the git repository from the URL:\url{https://bitbucket.org/christiealappatt/race-ad/src/master/}. It contains pre-compiled version of the RACE library and example kernels required for the sc18 poster. The main source code of the RACE library can be found under \url{https://bitbucket.org/christiealappatt/race/src/master/} which is expected to be made public by October 2018.
	
	\subsection{Hardware dependencies}
	Multicore processors. Currently RACE library has been tested only for Intel and AMD systems.
	
	\subsection{Software dependencies}
	RACE requires hwloc \cite{hwloc} library. If not found hwloc will be downloaded and installed. If RCM is used in the level construction phase (step 1 of RAC method) Intel SpMP \cite{SpMP} is also required. The library will also be downloaded and installed automatically in build process.
	
	\subsection{Datasets}
	24 of the matrices are from well-known SuiteSparse Matrix collection \cite{UOF} (former University of Florida Sparse Matrix collection). Rest 4 matrices come from quantum physics field and can be generated using GHOST and PHYSICS library \cite{essex_repo} which is publicly available.
	
	\section{Installation}
	\begin{enumerate}
		\item Clone the git repository.
	\end{enumerate}
	\begin{lstlisting}
		$ git clone\
		  git@bitbucket.org:christiealappatt/race-ad.git
		$ cd race-ad
	\end{lstlisting}
	\begin{enumerate}
		 \setcounter{enumi}{1}
		\item Configure the build using cmake. Set RACE\_USE\_SPMP for using RCM instead of BFS in level construction phase. This is done for experiment runs shown in poster.	
	\end{enumerate}
	\begin{lstlisting}
		$ mkdir build && cd build
		$ CC=icc CXX=icpc cmake .. -DRACE_USE_SPMP=ON
	\end{lstlisting}
	\begin{enumerate}
		\setcounter{enumi}{2}
		\item Compile two provided executables \texttt{race} and\\ \texttt{symm\_kacz\_convergence}. If dependencies are not installed this step will download and install them. Please ensure internet connectivity during the first make.
	\end{enumerate}
	\begin{lstlisting}
	$ make
	\end{lstlisting}
	
	\section{Experiment workflow}
	To run the performance benchmarks of SymmSpMV and SymmKACZ shown in the poster use the \texttt{race} executable.
	
	To do the convergence analysis of Symmetric Kaczmarz (SymmKACZ) use  \texttt{symm\_kacz\_convergence} executable. The workflow of the convergence test is as follows
	
	\begin{enumerate}
		\item Measure the L2 norm error reached by serial (1 thread) run for 1000 iterations. This is fixed as baseline.
		\item Now do the parallel run and record the iterations required to reach the same error as baseline.
	\end{enumerate}
	
	For comparison with Multicoloring (MC) we used COLPACK \cite{COLPACK} library to color (pre-processing step) the matrix. In case of Algebraic Block Multicoloring (ABMC) we used first METIS \cite{METIS} to partition the matrix into blocks and then we used COLPACK to color these blocks. A tuning was done from small (4) to large block size (128) as shown in \cite{ABMC}. Similar kernels to that of RACE was run with these methods. For comparison of SymmSpMV with MKL\cite{MKL} we used \texttt{mkl\_cspblas\_dcsrsymv()} routine. The test scenario and work flow was identical to that of RACE runs.

	
	To do hardware performance counter measurements we used LIKWID \cite{LIKWID}. This has been used to generate the plot of memory data traffic used in the poster.  
	
	\section{Evaluation and expected result}
	All runs shown in the poster were conducted with pinned threads and fixed core and uncore clock speed (2.4 GHz SkyLake and 2.2 GHz IvyBridge). While executing kernels with RACE it automatically pins the threads, but for kernels written outside RACE we need to pin using tools like \texttt{likwid-pin}. Since the measurements are done only on one socket its also sufficient to use \texttt{taskset}. 
	
	Pre-processing time were not included for any of the shown results. An efficiency (parameter) of 80\% (constant value) was fixed for each recursive stage of the RACE pre-processing.
	
	The performance results reported were mean of 1000 iterations. The result can be obtained by running the following command:
	\begin{lstlisting}
		$ OMP_NUM_THREADS=$nthreads RACE_EFFICIENCY=80,80\
		 taskset -c 0-$((nthreads-1)) ./race \
		 -m [matrix file] -c $nthreads -i 1000
	\end{lstlisting}
	During the run it displays the RAC tree structure created for the specific matrix. Finally it reports the performance in GFlop/s of different kernels like sparse matrix transpose vector, symmetric sparse matrix vector, kaczmarz, gauss-seidel etc. run with RACE. Performance of SpMV kernel is also reported for comparison.
	\begin{lstlisting}
	      SPMV :  16.4880 GFlop/s ; Time =  0.49739 s
	      SPMTV :  16.4264 GFlop/s ; Time =  0.49926 s
	      KACZ :  30.1878 GFlop/s ; Time =  0.54333 s
	      SYMM_KACZ :  27.9842 GFlop/s ; Time =  1.17224 s
	      GS :  16.4741 GFlop/s ; Time =  0.49781 s
	      SYMM_SPMV :  30.4742 GFlop/s ; Time =  0.26911 s
	\end{lstlisting}
	
	For obtaining the convergence results of symmKACZ one has to specify the maximum number of iterations and desired L2 error to be achieved. The run is as follows:
	\begin{lstlisting}
		$ OMP_NUM_THREADS=$nthreads RACE_EFFICIENCY=80,80\
		 taskset -c 0-$((nthreads-1)) ./symm_kacz_convergence\
		  -m [matrix] -c $nthreads -i [max. iter] -T [error] 
	\end{lstlisting} 
	
	The run would give back the performance, actual error and iterations achieved after the run.
	
	


