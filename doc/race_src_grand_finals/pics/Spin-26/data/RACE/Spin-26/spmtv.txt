THREAD,   PERF (GFlop/s),         MEM (GB),          L3 (GB),          L2 (GB),          FS (MB)
    1,         0.957826,          10.7351,          14.5921,          16.3141,           0.0032
    2,         1.821729,          11.5419,          14.5982,          16.3193,           0.3228
    3,         2.683980,          11.8983,          14.5954,          16.3189,           0.2352
    4,         3.523362,          12.1284,          14.5968,          16.3183,           0.1989
    5,         4.240939,          12.3157,          14.5948,          16.3179,           0.1775
    6,         4.487479,          12.4564,          14.6015,          16.3176,           0.1817
    7,         4.675032,          12.5841,          14.6081,          16.3174,           0.1922
    8,         4.550691,          12.6881,          14.6154,          16.3175,           0.2534
    9,         4.658527,          12.7542,          14.6172,          16.3175,           0.2412
   10,         4.675696,          12.8481,          14.6178,          16.3178,           0.2586
