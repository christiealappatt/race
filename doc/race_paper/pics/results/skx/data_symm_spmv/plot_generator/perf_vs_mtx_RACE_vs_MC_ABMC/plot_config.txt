COL_MTX_NAMES = 2
COL_PERF_1 = 5 5 5
PERF_FILES = ../../RACE/result.txt ../../ABMC/result.txt ../../MC/result.txt
COLORS =  orange cyan green citrine arylideyellow
LEGEND = RACE ABMC MC
TEMPLATE = template.tex
GENERATED = perf.tex
YLABEL = Perf (GF/s)
TYPE = LINE
TITLE = 
#SymmSpMV comparison on 1 socket of Intel Skylake


