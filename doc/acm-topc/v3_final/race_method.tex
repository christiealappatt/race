
Our advanced coloring algorithm is based on three steps:
\begin{enumerate}
	\item level construction,
	\item \DK coloring,
	\item load balancing.
\end{enumerate}
In the first step we apply a bandwidth reduction algorithm
including level construction and matrix reordering. We then use the
information from the level construction step to form subsets of levels
which allow for hardware-efficient \DK coloring of the graph. Finally
we present a concept to ensure load balancing between threads. These steps
are applied recursively when required.



To illustrate the method we choose a
simple matrix which is associated with an artificially constructed
two-dimensional stencil as shown in \Cref{fig:2d-7pt-a}. The
corresponding sparsity pattern and the graph of the matrix are shown
in \Cref{fig:2d-7pt-b,fig:2d-7pt-c} respectively.
\begin{figure}[t]
	\centering
	\subfloat[\Stex]{\label{fig:2d-7pt-a}\includegraphics[width=0.17\textwidth , height=0.13\textheight]{pics/2d-7pt/stencil.pdf}}
	\hspace{0.8em}
	\subfloat[Sparsity
	 pattern]{\label{fig:2d-7pt-b}\includegraphics[width=0.325\textwidth , height=0.185\textheight]{pics/2d-7pt/sparsity_2d_7pt_8x8.png}}
	\hspace{1em}
	\subfloat[Graph]{\label{fig:2d-7pt-c}\includegraphics[width=0.3\textwidth , height=0.18\textheight]{pics/2d-7pt/stencil_2d_7pt.pdf}}
	\caption{\sref{fig:2d-7pt-a} Structure of an artificially designed stencil,
	\sref{fig:2d-7pt-b} corresponding sparsity pattern of its matrix
	representation on an $8\times 8$ lattice with Dirichlet
	boundary conditions, and \sref{fig:2d-7pt-c} the graph representation of the
	matrix. The stencil
	structure was chosen for illustration purposes and does not
	represent any specific application scenario.}
	\label{fig:2d-7pt}
\end{figure}

\subsection*{Definitions}
We need the following definitions from graph theory:
\begin{itemize}
	\item \textbf{Graph: } $G = (V,E)$ represents a graph, with $V(G)$
              denoting its set of vertices and $E(G)$ denoting its edges. Note that
              we restrict ourselves to irreducible undirected graphs.
	\item \textbf{Neighborhood:} $N(u)$ is the neighborhood of a vertex $u$ and is defined as
	\begin{equation*}
	  N(u) = \set{ v \in V(G) : \{u,v\} \in E(G)}\eos
	 %N(u) = { v \in V(G) : (u,v) \in E(G)}
	\end{equation*}
	\item \textbf{$k$th Neighborhood:} $N^{k}(u)$ of a vertex $u$ is defined as
	 \begin{align*}
	 	N^2(u) &= N(N(u))  \\
	 	N^3(u) &= N^2(N(u)) \\
	 	\vdots\\
	 	N^k(u) &= N^{k-1}(N(u)) \eos
	 \end{align*}
	\item \textbf{Subgraph:} In this paper a subgraph $H$ of $G$ specifically
              refers to the subgraph induced by vertices $V' \subseteq V(G)$ and is defined as
	\begin{equation*}
		H = (V', \set{ \{u,v\} \in E(G) \text{ and } u,v \in V'})\eos
	\end{equation*}
\end{itemize}

\subsection{Level Construction}\label{subsec:LEVEL_CONST}

The first step of \acrshort{RACE} is to determine
different \textit{\levels} in the graph and permute the
graph data structure. This we achieve using
well-known bandwidth reduction algorithms such as \acrfull{RCM} \cite{RCM}
or \acrfull{BFS} \cite{BFS}\@. Although the RCM method is
also implemented in \acrshort{RACE}, we use the \acrshort{BFS}
reordering in the following for simpler illustration.


First we choose a \emph{root} vertex and assign it to the
first \level, $L(0)$\@. For $i>0$, \level \acrshort{L_i}
is defined to contain vertices that are in the neighborhood of vertices
in $L(i-1)$ but neither in $L(i-2)$  nor its neighborhood \cite{BFS_level_def}, \ie
\begin{equation}\label{eq:level}
L(i) = 
\begin{cases}
	 root & \text{ if } i = 0, \\
	 u : u \in N(L(i-1))  & \text{ if } i = 1, \\
	 u : u \in N(L(i-1)) \cap \overline{N(L(i-2))} \cap \overline{L(i-2)}  & \text{otherwise}.
\end{cases}   
\end{equation}
From \Cref{eq:level} one finds that the $i$th \level consists of all
vertices that have a minimum distance $i$ from the root node.
\Cref{alg:BFS} (see \Cref{Sec:algo}) shows how to determine this distance and thus set up the
\levels $L(i)$\@. We refer to the total number of \levels obtained for a particular graph
as \acrshort{totalLvl}. \Cref{fig:2d_7pt_level_construction} shows the
\acrshort{totalLvl}=14 \levels of our artificial stencil
operator, where the index of each vertex ($v$) is the
vertex number and the superscript represents the \level number, \ie
\begin{equation}\label{eq:node_notation}
	v^i \implies v \in L(i)\eos
\end{equation}
Note that the $L(i)$ are  substantially different from the \levels used in
the ``level-scheduling'' \cite{saad} approach, which applies ``depth first
search.''

\setlength{\fboxsep}{0pt}%

\begin{figure}[t]
	\centering
	\subfloat[Level construction]{\label{fig:2d_7pt_level_construction}\includegraphics[height=0.18\textheight,width=0.32\textwidth]{pics/level_construction/stencil_2d_7pt}
			\begin{picture}(0,0)
			\put(-44,52){\fbox{\includegraphics[height=1.4cm]{pics/level_construction/FDM_2d_7pt_non_perm}}}
			\end{picture}
		}
	\hspace{1em}
	\subfloat[Permuted graph ($G'$)]{\label{fig:2d_7pt_perm}\includegraphics[height=0.18\textheight,width=0.32\textwidth]{pics/permutation/stencil_2d_7pt}
			\begin{picture}(0,0)
			\put(-43.5,52){\fbox{\includegraphics[height=1.4cm]{pics/permutation/FDM_2d_7pt_perm}}}
			\end{picture}
		}
	\hspace{1em}
	\subfloat[]{\label{fig:2d_7pt_levelPtr}\includegraphics[height=0.18\textheight,width=0.07\textwidth]{pics/permutation/levelPtr}}
	\caption{\sref{fig:2d_7pt_level_construction} Levels of the original graph and \sref{fig:2d_7pt_perm} the permuted
          graph for the \stex. Insets show the corresponding sparsity patterns.
          \sref{fig:2d_7pt_levelPtr} Shows the entries of the \levelPtr array associated
          with $G'$.}
	\label{fig:2d-7pt_step_1_2}
\end{figure}



After the \levels have been determined, the matrix is permuted in the
order of its \levels, such that the vertices in $L(i)$ are stored
consecutively and appear before those of
$L(i+1)$. \Cref{fig:2d-7pt_step_1_2} shows the graph ($G' = P(G)$)
of the \stex after applying this permutation ($P$) and demonstrates
the enhanced spatial locality of the vertices within and between
\levels (see \Cref{fig:2d_7pt_perm}) as compared to the original
(lexicographic) numbering (see \Cref{fig:2d_7pt_level_construction}).
Until now the procedure is the same as \acrshort{BFS} (or
\acrshort{RCM}).

As \acrshort{RACE} uses information about the \levels for resolving
dependencies in the coloring step, we store the index of the entry point to each
\level in the permuted data structure (of $G'$) in an array
$\levelPtr[0:$ \acrshort{totalLvl}$]$, so that \levels on $G'$ can be
identified as
\begin{equation*}
  L(i) = \set{ u : u \in [\levelPtr[i]:(\levelPtr[i+1]-1)]
    \text{ and } u \in V(G')}\eos
\end{equation*}
The entries of \levelPtr for the \stex are shown in \Cref{fig:2d_7pt_levelPtr}. 
 
\subsection{Distance-k coloring} \label{subsec:DK}

The data structure generated above serves as the basis for our \DK coloring
procedure as it contains information about the neighborhood relation
between the vertices of any two \levels. Following the definition
in~\cite{dist_k_def}, two vertices are called \DK neighbors if the
shortest path connecting them consists of at most $k$ edges.
This implies that $u$ is a \DK neighbor of $v$ (referred to as
$u\xrightarrow{k}v$) if
\begin{equation}\label{eq:dk}
  u\xrightarrow{k}v  \iff  v \in \set{ u \cup N(u) \cup N^2(u) \cup \cdots N^k(u) }\eos
\end{equation}
For the undirected graphs as used here, $u\xrightarrow{k}v$
also implies $v\xrightarrow{k}u$. Based on this definition we consider
two vertices to be \DK independent if they are not \DK
neighbors, and two \levels are said to be \DK independent 
if their vertices are mutually \DK independent.
Thus, \levels $L(i)$ and $L(i\pm(k+j))$ $\forall j\geq1$ of 
the permuted graph $G'$ are \DK independent, 
denoted as
\begin{equation}\label{corollary_dk}
	L(i) \not{\xrightarrow{k}} L(i\pm(k+j)) \forall j\geq1 \eos
\end{equation} 
\begin{comment}
 as shown in the
following.
\begin{corollary}\label{corollary_dk}
$L(i)$ and $L(i\pm(k+j))$ are \DK independent $\forall j\geq1$. 
\end{corollary}
\begin{proof}
  We prove by contradiction. Let there exist $u,v \in V(G')$ such that
  $u \in L(i)$ and $v \in \bigcup\limits_{j\geq 1}L(i \pm (k+j))$. Assume $u,v$
  are \DK neighbors ($u\xrightarrow{k}v$). From \Cref{eq:level},
  \Cref{eq:dk}, and the fact $G'$ is undirected we get
\begin{align*}
  u\xrightarrow{k}v \iff & v \in \set{L(i) \cup L(i \pm 1) \cup \cdots \cup L(i \pm k)} \\
  \implies & v \notin L(i \pm (k+j)) \text{  } \forall j \geq 1\cma
\end{align*}
which contradicts our assumption about $v$\@.
Thus it follows that $u$ and $v$ are \DK independent.
\end{proof}
\end{comment}
\Cref{corollary_dk} implies that if there is a gap of at least
one \level between any two \levels (e.g., $L(i) \mbox{ and } L(i+2)$) all
pairs of vertices between these two levels are \DONE independent. Similarly
if the gap consists of at least two \levels between any two
\levels (e.g., $L(i) \mbox{ and } L(i+3)$) we have \DTWO independent
\levels, and so on.
 \begin{figure}[t]
 	\centering
 	\subfloat[Distance-1 independent \levelGroups]{\label{fig:2d_7pt_d1}\includegraphics[height=0.2\textheight,width=0.4\textwidth]{pics/dk_coloring/stencil_2d_7pt_d1}}
 	\hspace{2.5em}
 	\subfloat[Distance-2 independent \levelGroups]{\label{fig:2d_7pt_d2}\includegraphics[height=0.23\textheight,width=0.48\textwidth]{pics/dk_coloring/stencil_2d_7pt_d2_with_lg}}
 	\caption{Forming \DONE and \DTWO independent \levelGroups for the \stex.}
 	\label{fig:2d-7pt_d1_d2}
 \end{figure}
 
 
The definition used in \Cref{corollary_dk} offers many choices for
forming \DK independent sets of vertices, which can then be executed
in parallel.  In \Cref{fig:2d-7pt_d1_d2} we present one example each
for \DONE (\Cref{fig:2d_7pt_d1}) and \DTWO (\Cref{fig:2d_7pt_d2})
colorings of our \stex. The \DONE coloring uses a straightforward
approach by assigning two colors to alternating \levels, i.e., \levels
of a color can be calculated concurrently. In case of \DTWO
independence we do not use three colors but rather aggregate two
adjacent \levels to form a \textit{\levelGroup} (denoted by
\acrshort{T_i}) and perform a \DONE coloring on top of those
groups. This guarantees that vertices of two \levelGroups of the same
color are \DTWO independent and can be executed in parallel. Here, the
vertices in $T(0)$, $T(2)$, $T(4)$, and $T(6)$ can be operated on by
four threads in parallel, i.e., one thread per \levelGroup.  After
synchronization the remaining four blue \levelGroups can also be
executed in parallel. This idea can be generalized such that for \DK
coloring, each \levelGroup contains $k$ adjacent \levels.
Thus formed \levelGroups are then \DONE
colored. Then, all \levelGroups within a color can be executed in
parallel. This simple approach allows one to generate workload for
a maximum of ${\acrshort{totalLvl}}/{2 k}$ threads if \DK coloring is
requested.\footnote{This implies that as the number of levels increases,
so does the parallelism. 
On the other hand, if the matrix contains at least one row that is
significantly longer than the average, parallelism may be very limited 
(e.g., for link matrices like com-Orkut from the SuiteSparse Matrix Collection). For a completely dense row we have $\acrshort{totalLvl}=2$.} 
In all cases, vertices within a single \levelGroup
are computed in their BFS/RCM permuted order,
which allows for good spatial locality if the level groups are sufficiently
  large. Note that keeping level groups large is one of the major
advantages of RACE. 
 
Choosing the same number of \levels for each \levelGroup may,
however, cause severe load imbalance depending on the matrix structure. In
particular, the use of bandwidth reduction schemes such as BFS or RCM
will further worsen this problem due to the lenslike shape of the
reordered matrix (see inset of \Cref{fig:2d_7pt_perm}), leading to low
workload for \levelGroups containing the top and bottom rows of the
  matrix. Compare, \eg $T(0)$ and $T(7)$ with $T(3)$ and $T(4)$ in
\Cref{fig:2d_7pt_d2}. However, \Cref{corollary_dk} does not require
\emph{exactly} $k$ levels to be in a \levelGroup but  
only \emph{\atleast} $k$. In
the following we exploit this to alleviate the imbalance
problem.

\subsection{Load balancing}\label{subsec:LB} 
The RACE load balancing scheme tries to balance the workload across level
groups within each color for a given number of threads while maintaining data
locality and the \DK constraint between the two colors. To achieve this we use
an idea similar to incremental graph partitioning \cite{load_balancing}.  The
\levelGroups containing low workload ``grab'' adjacent levels from neighboring
level groups; overloaded \levelGroups shift levels to adjacent \levelGroups.
One can either balance the number of rows (\ie
vertices) $\acrshort{nrows}(T(i))$ or the number of
nonzeros (\ie edges)  $\acrshort{nnz}(T(i))$. Both variants
are supported by our implementation, and we choose balancing by number of
rows in the following to demonstrate the method (see
\Cref{alg:LB}).
 
For a given set of \levelGroups we calculate the mean and variance of
$\acrshort{nrows}(T(i))$ within each color (red and blue).
 %For example in \Cref{fig:2d_7pt_d2} we need to calculate mean of $T\_size$ of
The overall variance,
which is the target of our minimization procedure, is then found by summing up
the variances across colors. \Inorder to reduce this value we first select the
two \levelGroups with largest negative/positive deviation from the mean (which
is $T(5)$ and $T(4)$ in step 1 of \Cref{fig:lb_alg}) and try to add/remove
levels to/from them (see top row of \Cref{fig:lb_alg}). When
removing \levels from a \levelGroup, the \DK coloring is strictly maintained 
by keeping at least $k$ levels in it. The shift of
\levels is done with the help of an array  $T\_ptr[]$, which holds pointers to the
beginning of each \levelGroup (see \Cref{fig:lb_alg}), avoiding any copy
operation. If shifting levels between the two level groups with the largest
deviation does not lead to a lower overall variance, no levels are exchanged and
we choose the next pair of level groups according to a ranking which is based on
the absolute deviation from the mean (see \Cref{alg:LB} for implementation
details) and continue. Following this process in an iterative way we finally end
up in a state of lowest overall variance at which no further moves are
possible, either because they would violate the \DK dependency or lead to
an increase in overall
variance. \Cref{fig:lb_alg} shows the load balancing procedure under a \DTWO
constraint for some initial mapping of 17 levels to six \levelGroups. Applying
the procedure to our \stex of size $16 \times 16$, requesting \DTWO coloring and
ten level groups leads to the mapping shown in \Cref{fig:2d_7pt_lb}. 
 The \levelGroups at the extreme ends have more \levels due to fewer vertices
(\acrshort{nrows}) in each \level, while \levelGroups in the middle, having more
vertices, maintain two levels to preserve the \DTWO constraint.

   \begin{figure}[t]
       \centering
       \includegraphics[width=\textwidth]{pics/load_balancing/lb_alg/lb_all}
       \caption{All steps of the load balancing scheme, applied to an
          arbitrarily chosen initial distribution of 17 levels into six level
          groups for \DTWO coloring. Rebalancing steps are performed clockwise
          starting from top left. $mean\_r$ and $mean\_b$ denote the current
          average number of rows per \levelGroup and color. $var$ is the overall
          variance.}
       \label{fig:lb_alg}
   \end{figure}

   \begin{figure}[t]
       \centering
        \subfloat[Five threads]{\label{fig:2d_7pt_lb}\includegraphics[width=0.48\textwidth, height=0.28\textheight]{pics/load_balancing/2d-7pt/stencil_2d_7pt}}
        \hspace{0.2em}
        \subfloat[Eight threads]{\label{fig:2d_7pt_lb_8_threads}\includegraphics[width=0.48\textwidth, height=0.28\textheight]{pics/load_balancing/2d-7pt/stencil_2d_7pt_8_threads}}
         \caption{Domain size $16 \times 16$ for the \stex and \DTWO dependency, \sref{fig:2d_7pt_lb} after load balancing for five threads, 
         %Note that \levelGroups at extreme end have more \levels due to fewer \acrshort{nrows} in each \level, while \levelGroups in the middle having bigger \levels maintain two levels to preserve \DTWO constraint. 
         \sref{fig:2d_7pt_lb_8_threads} after load balancing for eight threads.
        }
   \end{figure}



	\subsection{Recursion}\label{subsec:REC}
As discussed in \Cref{subsec:DK}, the maximum degree of parallelism is limited
by the total number of levels (\acrshort{totalLvl}) and may be further reduced 
by level aggregation. In case of our
$16\times16$ stencil example the maximum possible parallelism 
is eight threads, which may cause load imbalance as seen in 
\Cref{fig:2d_7pt_lb_8_threads}.
Hence, %to match the high levels of parallelism in modern compute devices 
further parallelism must be found within the \levelGroups.
Compared to methods like \acrshort{MC} 
we do not require all vertices in a \levelGroup to be \DONE (or \DK in general) 
independent. This is a consequence of our \level-based approach, which
requires \DK independence between vertices of different levels but not 
within a level (see \Cref{corollary_dk}). There may be more parallelism hidden within 
the \levelGroups,  which can be interpreted as \subgraphs.  Thus we apply the three 
steps of our method recursively on selected \subgraphs to exploit the parallelism 
within them.  

In the following section we first demonstrate the basic
idea in the context of \DONE dependencies, which can be resolved
within the given \levelGroup by design. However, for $k>1$, vertices in a
\levelGroup may have \DK dependencies via vertices in adjacent
\levelGroups. We generalize our procedure to \DK dependencies as a second step
in \Cref{subsec:Dk_dependency}. Finally, in \Cref{subsec:subgraph_selection} we
apply the recursive scheme to our \stex and introduce proper \subgraph selection
as well as global load balancing strategies.

In order to visualize the basic concepts easily and discuss important corner
cases of the recursive approach we start with the simple graph shown in
\Cref{fig:rec_d1_s1_a}, which is not related to our \stex. To distinguish
between \levelGroups at different stages \acrshort{s} of the recursive procedure
we add a subscript to the levels (\acrshort{L_si}) and \levelGroups
(\acrshort{T_si}) indicating the stage of recursion at which they are generated,
with $s=0$ being the original distribution before recursion is applied to any
\subgraph.

	
	\subsubsection{Distance-$1$ dependency} \label{subsec:D1_dependency}

For the \DONE coloring of the graph in \Cref{fig:rec_d1_s1} we find that
three of the four \levelGroups of the initial stage still contain \DONE-independent vertices; e.g., in $T_0(2)$ we have vertices $3 \not{\xrightarrow{1}} 4$ ($3$ \DONE
independent to $4$), $3 \not{\xrightarrow{1}} 5$, $3 \not{\xrightarrow{1}} 6$,
and $4 \not{\xrightarrow{1}} 6$, implying each of these pairs can be computed in
parallel without any \DONE conflicts. This parallelism is not exposed at
the first stage ($s=0$) as vertices in $L_0(i)$ are chosen such
that they are \DONE neighbors of $L_0(i-1)$, ignoring any vertex relations
\emph{within} $L_0(i)$.
     \begin{figure}[t]
     	\centering
     	\subfloat[Example graph]{\label{fig:rec_d1_s1_a}\includegraphics[width=0.26\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s1/recursion_graph_1}}
     	\hspace{1.5em}
     	\subfloat[Stage 0, levels in graph]{\label{fig:rec_d1_s1_b}\includegraphics[width=0.32\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s1/recursion_graph_2}}
     	\hspace{1.5em}
     	\subfloat[\DONE coloring]{\label{fig:rec_d1_s1_c}\includegraphics[width=0.32\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s1/recursion_graph_3}}
        \caption{Exposing potential for more parallelism in a graph with \DONE coloring.
          $T_0(1),T_0(2),$ and
          $T_0(3)$ have internal unexposed parallelism.
          Note that the graph shown here is not
          related to the previous \stex.}
     	\label{fig:rec_d1_s1}
     \end{figure}

Recursion starts with the selection of a \subgraph of the matrix, which is
discussed in more detail later (see \Cref{subsec:subgraph_selection}). Here we
choose the \subgraph induced by $T_0(2)$. It can be isolated from the
rest of the graph since the \DONE coloring step in stage 0 has already produced
independent \levelGroups. Now we just need to repeat the three
steps explained previously (\Cref{subsec:LEVEL_CONST}--\Cref{subsec:LB}) on this
\subgraph.
     \begin{figure}[t]
     	\centering
     	\subfloat[]{\label{fig:rec_d1_s2_a}\includegraphics[width=0.28\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s2/recursion_graph_stage2_1}}
     	\hspace{2.25em}
     	\subfloat[]{\label{fig:rec_d1_s2_b}\includegraphics[width=0.07\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s2/recursion_graph_stage2_2}}
     	\hspace{1.75em}
     	\subfloat[]{\label{fig:rec_d1_s2_c}\includegraphics[width=0.065\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s2/recursion_graph_stage2_3}}
     	\hspace{1.75em}
     	\subfloat[]{\label{fig:rec_d1_s2_d}\includegraphics[width=0.065\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s2/recursion_graph_stage2_4}}
	     \hspace{1.75em}
	     \subfloat[]{\label{fig:rec_d1_s2_e}\includegraphics[width=0.065\textwidth, height=0.14\textheight]{pics/recursion/d1/rec_graph_s2/recursion_graph_stage2_5}}
     	\caption{Applying recursion to the \subgraph induced by
          $T_0(2)$. \Cref{fig:rec_d1_s2_b} shows the isolated \subgraph,
          while \Cref{fig:rec_d1_s2_c} presents the level construction step on the
          \subgraph. Two potential \DONE colorings of this \subgraph are shown
          in \Cref{fig:rec_d1_s2_d,fig:rec_d1_s2_e}.}
     	
     	\label{fig:rec_d1_s2}
     \end{figure}
     
     \Cref{fig:rec_d1_s2} shows an illustration of applying the first recursive step ($s=1$) on $T_0(2)$, where we extend the definition of the vertex numbering in \Cref{eq:node_notation} to the following:
	 \begin{equation}
	    v^{i,j,k...} \implies v \in \set{L_0(i) \cap L_1(j) \cap L_2(k) \cap \cdots}.
	 \end{equation}
At the end of the recursion (\cf
\Cref{fig:rec_d1_s2_d,fig:rec_d1_s2_e}) on $T_0(2)$, we obtain parallelism for
two more threads in this case.
The \subgraphs might have ``islands'' (groups of vertices
that are not connected to the rest of the graph); \eg vertex 3 and vertices
4,5,6 form two islands in \Cref{fig:rec_d1_s2_b}. Since an island is 
disconnected from the rest of the (sub)graph it can be executed independently
and in parallel to it. To take advantage of this, the starting node in the
next island is assigned a level number with an increment of two, as seen in
\Cref{fig:rec_d1_s2_c}. This allows for two different colorings of the island,
increasing the
number of valid \DONE configurations (\cf
\Cref{fig:rec_d1_s2_d,fig:rec_d1_s2_e}). The selection of the optimal
one will be done in the final load balancing step as described in
\Cref{subsec:LB}.
     
As this recursive process finds independent \levelGroups
($T_{s+1}$) within a \levelGroup of the previous stage ($T_s$), the
thread assigned to $T_s$ has to spawn threads to parallelize within
$T_{s+1}$.

\subsubsection{Distance-$k$ dependencies with $k>1$}  \label{subsec:Dk_dependency}

In general, it is insufficient to consider only
the \subgraphs induced by \levelGroups in the recursion step, as can be seen in
\Cref{fig:rec_d2_wrong_a} for \DTWO coloring. Applying the three steps (see
\Cref{fig:rec_d2_wrong_b,fig:rec_d2_wrong_c,fig:rec_d2_wrong_d}) to the
\subgraph induced by $T_0(1)$ does not guarantee \DTWO independence between the
new \levelGroups $T_1(0)$ and $T_1(2)$. It is obvious that for general \DK
colorings two vertices $a,b$ within a \levelGroup might be connected by a shared
vertex $c$ outside the \levelGroup.  Thus, our three step procedure must be
applied to a \subgraph which contains the actual \levelGroup ($T_s(j)$) as well
as its all distance-$p$ neighbors, where $p=1,2,\ldots,(k-1)$.
     \begin{figure}[t]
     	\centering
     	\subfloat[]{\label{fig:rec_d2_wrong_a}\includegraphics[width=0.23\textwidth, height=0.13\textheight]{pics/recursion/d2/wrong/recursion_graph_wrong_1}}
     	\hspace{0.6em}
     	\subfloat[]{\label{fig:rec_d2_wrong_b}\includegraphics[width=0.23\textwidth, height=0.07\textheight]{pics/recursion/d2/wrong/recursion_graph_wrong_2}}
     	\hspace{0.6em}
     	\subfloat[]{\label{fig:rec_d2_wrong_c}\includegraphics[width=0.23\textwidth, height=0.07\textheight]{pics/recursion/d2/wrong/recursion_graph_wrong_3}}
     	\hspace{0.6em}
     	\subfloat[]{\label{fig:rec_d2_wrong_d}\includegraphics[width=0.23\textwidth, height=0.13\textheight]{pics/recursion/d2/wrong/recursion_graph_wrong_4}}
     	\caption{Two \levelGroups generated by a \DTWO coloring
          (\Cref{fig:rec_d2_wrong_a}). \Cref{fig:rec_d2_wrong_b} shows the
          \subgraph induced by \levelGroup $T_0(1)$. Level construction on the
          selected \subgraph is shown in \Cref{fig:rec_d2_wrong_c}. Forming
          \DTWO independent \levelGroups on these levels does not guarantee a
          \DTWO independence between the newly generated \levelGroups of the
          same color as seen in \Cref{fig:rec_d2_wrong_d}.}
     	\label{fig:rec_d2_wrong}
     \end{figure}


This ensures that there is no vertex outside the \subgraph which can mediate a
\DK dependency between vertices in the embedded \levelGroup ($T_s(j)$). We can
now construct the new levels ($L_{s+1}(:)$) on this \subgraph considering the neighborhood, 
but in $L_{s+1}(:)$
we only store the vertices 
 that are in the
embedded \levelGroup ($T_s(j)$). Next we apply \DK coloring by aggregation of
the new levels, leading to a set of \levelGroups $T_{s+1}(:)$ within
$T_s(j)$. \Cref{fig:rec_d2_correct} demonstrates this approach to resolve the conflict
shown in \Cref{fig:rec_d2_wrong_d}. \Cref{fig:rec_d2_correct_b} presents
the \subgraph containing the selected \levelGroup $T_0(1)$ and its \DONE
neighborhood.

\begin{figure}[t]
     	\centering
     	\subfloat[]{\label{fig:rec_d2_correct_a}\includegraphics[width=0.22\textwidth, height=0.13\textheight]{pics/recursion/d2/correct/recursion_graph_correct_1}}
     	\hspace{0.6em}
     	\subfloat[]{\label{fig:rec_d2_correct_b}\includegraphics[width=0.22\textwidth, height=0.105\textheight]{pics/recursion/d2/correct/recursion_graph_correct_2}}
     	\hspace{0.6em}
     	\subfloat[]{\label{fig:rec_d2_correct_c}\includegraphics[width=0.22\textwidth, height=0.105\textheight]{pics/recursion/d2/correct/recursion_graph_correct_3}}
     	\hspace{0.6em}
     	\subfloat[]{\label{fig:rec_d2_correct_d}\includegraphics[width=0.22\textwidth, height=0.105\textheight]{pics/recursion/d2/correct/recursion_graph_correct_4}}
     	\hspace{0.6em}
     	\caption{Correct procedure for \DTWO coloring of \levelGroup
          $T_0(1)$. The \subgraph as shown in \Cref{fig:rec_d2_correct_b}
          contains \levelGroup $T_0(1)$ and its \DONE neighborhood. A level
          construction step is applied to this \subgraph in
          \Cref{fig:rec_d2_correct_c}. Distance-2 coloring by level aggregation
          leading to \levelGroups of stage 1 is shown in
          \Cref{fig:rec_d2_correct_d}; we get three \levelGroups at the end of
          the recursion on $T_0(1)$.}
     	\label{fig:rec_d2_correct}
     \end{figure}
Level construction is performed on the \subgraph (\Cref{fig:rec_d2_correct_c}),
but the new levels only contain vertices of $T_0(1)$, \ie $L_1(1) =
\{7^{3,1}\}$. Finally, \DTWO coloring by aggregation of two adjacent levels is
performed, leading to three \levelGroups of the second stage $s=1$
(\Cref{fig:rec_d2_correct_d}), \ie $T_1(0)=\{L_1(0) \cup L_1(1)\}$.  Now
vertices $3$ and $6$ are mapped to \levelGroups of different colors. Note that
the permutation step on the newly generated levels is not shown but is performed
as well to maintain data locality.
    
       \begin{figure}[t]
       	\begin{minipage}[c]{0.6\textwidth}
       		\includegraphics[height=0.3\textheight,width=0.89\textwidth]{pics/recursion/2d-7pt_example/2d-7pt/stencil_2d_7pt}
       	\end{minipage}\hfill
       	\begin{minipage}[c]{0.4\textwidth}
	       	\begin{tabular}{l|l}
	       		{Initial Stage} & {Recursion}\\
	       		{($s=0$)} & {($s=1$)}\\
	       		\midrule
	       	   \multirow{2}{*}{\textcolor{red}{red}} & {\textcolor{amber}{orange}}\\
	       	   \rule{0pt}{3ex}
	       		& {\textcolor{magenta}{pink}}\\
				\rule{0pt}{4ex} 
	       	   \multirow{2}{*}{\textcolor{blue}{blue}} & {\textcolor{carmine}{brown}}\\
	       	   \rule{0pt}{3ex}
	       	   & {\textcolor{cyan}{cyan}}\\
	       	\end{tabular}
	       	\begin{tikzpicture}[overlay]
		       	\draw[-, dashed, red, line width=1.5] (-1.8,0.22) -- (-0.1,0.22);
		       	\draw[-, dashed, red, line width=1.5] (-3.9,-0.4) -- (-0.1,-0.4);
		      	\draw[-, dashed, red, line width=1.5] (-1.8,-1.05) -- (-0.1,-1.05);
		       	\draw[-, dashed, red, line width=1.5] (-3.9,-1.6) -- (-0.1,-1.6);
		       	\draw[->] (0.2,0.7) -- (0.2,-1.5);
		       	\node[rotate=-90] at (0.45,-0.4) {execution time};
	       	\end{tikzpicture}
       		\caption{Graph coloring of the \stex for eight
                  threads. Recursion is applied on \levelGroups $T_0(4-7)$ with
                  two threads assigned to each. The parallel execution order is
                  shown on the right.  Horizontal red dotted lines indicate
                  synchronization and its extent. Vertical lines distinguish
                  between \levelGroups of different stages (here $T_0$ and
                  $T_1$) which can run in parallel.
		}
       		\label{fig:rec_2d-7pt_graph}
       	\end{minipage}
       \end{figure}
     
          
\subsubsection{Level group construction and global load balancing} \label{subsec:subgraph_selection}

The recursive refinement of \levelGroups allows us to tackle load imbalance
problems and limited degree of parallelism as we are no longer restricted by the
one thread per \levelGroup constraint.  Instead, we have the opportunity to form
\levelGroups and assign appropriate thread counts to them such that the load per
thread approaches the optimal value, \ie the total workload divided by the number of
threads available. Pairs of adjacent level groups having different colors within a stage,
\ie $T_s(i)$ and $T_s(i+1)$ with $i=0,2,4,...$,
are typically handled by the same threads, so we assign an equal number of threads
to these \levelGroups. 
We then apply recursion to the \levelGroups with more than one
thread assigned. Starting with the original graph as the base \levelGroup
($T_{-1}(0)$) to which all available threads
$\acrshort{nthreads}(T_{-1}(0))=\acrshort{nthreads}$ and all vertices
$\acrshort{nrows}(T_{-1}(0))=\acrshort{nrows}^\mathrm{total}$ are assigned, we perform
the following steps to form \levelGroups $T_s(:)$ at stage $s \ge 0$ to which we
assign $\acrshort{nthreads}(T_{s}(:))$ threads. To illustrate the procedure we
use the $16 \times 16$ \stex and construct a coloring scheme for eight threads
(see \Cref{fig:rec_2d-7pt_graph}).
\begin{enumerate}
\item Assign weights to all levels at stage ($s$) of the recursion. Assuming
  that $L_s(i) \subset T_{s-1}(j)$, its weight is defined by
	\begin{align*}
		w(L_s(i)) &=
              %  = 
              \frac{\acrshort{nrows}(L_s(i))}{\acrshort{nrows}(T_{s-1}(j))}
                \acrshort{nthreads}(T_{s-1}(j)).\\
	\end{align*}
	
For a given \levelGroup ($T_{s-1}(j)$) that has to be split up
($\acrshort{nthreads}(T_{s-1}(j)) > 1$), the weight describes the fraction of
the optimal load per thread,
$\frac{\acrshort{nrows}(T_{s-1}(j))}{\acrshort{nthreads}(T_{s-1}(j))}$, in the
specific level ($L_s(i)$).

Requesting $\acrshort{nthreads}(T_{-1}(0))=8$ threads for the
$\acrshort{nrows}(T_{-1}(0)) = 256$ vertices of the \stex in
\Cref{fig:rec_2d-7pt_graph} produces the following weights for the initial
($s=0$) levels:
	\begin{align*}
		\{w(L_0(0)), w(L_0(1)), w(L_0(2)), ...\} &= \Big{\{} \frac{1}{256} \cdot 8 , \frac{2}{256} \cdot 8 , \frac{3}{256} \cdot 8 , ...\Big{\}}.
	\end{align*}
	
\item The above definition implies that if the weight is close to a natural
  number $b$, the corresponding workload is near optimal for operation with $b$
  threads. Thus, starting with $L_s(0)$ we aggregate successive levels until
  their combined weight forms a number $a$ close to a natural number
  $b$. Distance-k coloring is ensured by enforcing it to aggregate \atleast $2
  \cdot k$ \levels, \ie for \DTWO coloring at least four levels (two for red
  and two for blue). Closeness to the natural number is quantified by a parameter
  $\epsilon$ defined as
	\begin{align*}
		\epsilon =  1 - \abs(a-b), & \text{ where } b= \max(1,[a])\\
				& \text{and } [a] \text{ is the nearest integer to $a$},
	\end{align*}
	and controlled by the criterion
	\begin{align*}
	\epsilon &> \epsilon_s, \text{where the $\epsilon_s \in  [0.5,1)$ are user defined parameters.} 	
	\end{align*}		   
The choice of this parameter may be different for every stage of recursion.
Once we find a collection of successive levels satisfying this criterion, the
natural number $b$ is fixed. We try to further increase the number of levels to
test if there exists a number $a'>a$ which is closer to $b$ leading to an
$\epsilon$ value closer to one. We finally choose the set of levels with the
best $\epsilon$ value and define them to form $T_s(0)$ and $T_s(1)$ which are to
be executed by $\acrshort{nthreads}(T_s(0))=\acrshort{nthreads}(T_s(1))=b$
threads.  In \Cref{fig:rec_2d-7pt_graph} we choose $\epsilon_s = 0.6$, which
selects the first seven levels to form $T_0(0)$ and $T_0(1)$.  As their combined
weight is $\frac{28}{32}=0.875$, one thread will execute these two \levelGroups.

\item We continue with subsequent pairs of \levelGroups ($T_s(i), T_s(i+1);
  i=2,4 ...$) by applying this procedure starting with the very next
  \level. Finally, once all the levels have been touched, a total of
  $\acrshort{nthreads}(T_{s-1}(j))$ threads have been assigned to the levels
  $L_s(i) \subset T_{s-1}(j)$. For example, for $T_0(4)$ and $T_0(5)$ in
  \Cref{fig:rec_2d-7pt_graph} two threads satisfy the criterion as the total
  weight of the four levels included is $\frac{54}{32}=1.69$.
	
\item The distribution between adjacent red and blue \levelGroups which are
  assigned to the same thread(s) as well as the final global load balancing is
  performed using a slight modification of the scheme presented in \Cref{subsec:LB}
  (shown at the beginning of \Cref{alg:LB} in \Cref{Sec:algo}):
now the calculation of mean and variance must consider the number of threads
($\acrshort{nthreads}(T_{s}(j))$) assigned to each \levelGroup. 
The worker array now has to be replaced by the number of
threads assigned to each \levelGroup ($\acrshort{nthreads}(T_{s}(j))$). The
algorithm then tries to minimize the variance of the number of vertices per
thread in \levelGroups. Ideally, after
this step the load per thread in each \levelGroup should approach the optimal
value given above.
\end{enumerate}
Once the \levelGroup of stage $s$ has been formed, the recursion and the above
procedure are separately applied to all new \levelGroups with more than one
thread assigned. This continues until every \levelGroup is assigned to one
thread. The depth of the recursion is determined by the parameter $\epsilon_s$
and depends on the matrix structure as well as degree of parallelism requested.

For our \stex in \Cref{fig:rec_2d-7pt_graph} the inner four \levelGroups of
stage $s=0$ required one stage of recursion. This led to 16 \levelGroups at
stage $s=1$, as we require four new \levelGroups per recursion to schedule two
threads.  In terms of parallel computation, first the red vertices will be
computed in parallel with the orange ones using four threads for both
colors. Once the orange vertices are done, each pair of threads assigned to
$T_0(4)$ and $T_0(6)$ synchronize locally (\ie within $T_0(4)$ and $T_0(6)$
separately). Then the pink vertices are computed followed by a global
synchronization of all threads. The scheme continues with the blue vertices and
the brown/cyan ones, which represent the two blue \levelGroups to which
recursion has been applied (see table in \Cref{fig:rec_2d-7pt_graph}).

The recursive nature of our scheme can be best described by a tree data
structure, where every node represents one \levelGroup and the maximum depth is
equivalent to the maximum level (stage) of recursion. The data structure for the
colored graph in \Cref{fig:rec_2d-7pt_graph} and its 
static\footnote{The thread assignment is initially done in a static manner,
	which eliminates the overhead from dynamic work allocation during execution
	 and helps preserve data locality.}
thread assignments are
shown in \Cref{fig:rec_2d-7pt_tree}.
 The root node represents our baseline
\levelGroup $T_{-1}(0)$ comprising all 256 vertices and all eight threads
(having unique $id=0,\ldots,7$). The first level of child nodes gives the
initial ($s=0$) distribution, with each node storing the information of a
\levelGroup including its color. Threads are mapped consecutively to the
\levelGroups. The red $T_0(4)$ \levelGroup, which consists of
vertices $66,\ldots,90$ (omitting the superscript for level numbers), is executed
by threads with $id=2,3$.  Applying recursion to $T_0(4)$, this node spawns four
new child nodes at stage $s =1$, \ie \levelGroups $T_1(0,\ldots,3) \subset
T_0(4)$, to be executed by the two threads. Synchronization only happens between
threads having the same parent node after executing the same color. 
The actual computations are only performed on the leaf nodes of the final tree.
	 \begin{figure}[t]
		 \includegraphics[width=\textwidth, height=0.2\textheight]{pics/recursion/2d-7pt_example/tree/tree}
	 	\caption{The internal tree structure of \acrshort{RACE}
                  representing the \stex for domain size $16 \times 16$ and
                  eight threads. The range $[\ldots]$ specified in each leaf
                  represents the vertices belonging to each \levelGroup and the
                  id refers to the thread id assigned to each \levelGroup
                  assuming compact pinning . The last entry
                  $\langle\acrshort{nrowsEff}\rangle$ gives the effective row
                  count introduced in \Cref{Sec:param_study}. }
	 	\label{fig:rec_2d-7pt_tree}
	 \end{figure}
