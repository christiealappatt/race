THREAD             spmv         spmv_mkl            spmtv        symm_spmv               gs             kacz
    1         2.173955         2.181078         2.210418         3.326717         2.276163         3.024924
    2         4.280929         4.392803         4.147547         6.300494         4.301746         5.913466
    3         6.174545         6.314714         6.104246         9.220291         6.202311         8.563823
    4         7.973061         8.089601         7.845287        11.943968         8.012808        10.953037
    5         9.560732         9.724162         9.593599        14.593750         9.615671        13.505729
    6        11.084141        11.355729        11.304279        17.213003        11.356927        15.979761
    7        12.532375        12.782141        12.832413        19.691905        12.755060        18.361022
    8        13.949115        14.258246        14.054122        21.782745        13.946221        20.219321
    9        15.210144        15.567404        15.201342        23.261100        15.053597        21.922263
   10        16.164262        16.629940        16.289752        25.446090        15.990925        24.066480
   11        17.145391        17.474443        17.418089        27.862613        17.144324        26.226292
   12        18.006342        18.036190        17.976207        29.107933        17.844474        27.251735
   13        18.565574        18.946997        18.181605        29.446672        18.103935        27.012784
   14        19.002989        19.257128        19.324338        32.105419        19.290783        30.177642
   15        18.856363        19.152182        19.377988        32.742461        19.342615        30.959277
   16        19.420429        19.850681        19.447997        33.391120        19.312678        31.942220
   17        19.483674        19.912068        19.573470        34.737704        19.445384        32.484238
   18        19.638975        20.141861        20.217770        36.008005        20.040356        34.753519
   19        19.293577        19.613150        19.774879        36.968316        19.747892        35.120255
   20        18.965761        19.257448        19.771833        37.526387        19.678261        35.772628
