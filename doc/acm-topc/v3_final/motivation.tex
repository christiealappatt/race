The parallelization of
the \acrshort{SymmSpMV} kernel can be done
via \DTWO coloring of the corresponding graph. The computational
intensity, and hence the performance, depends on the data access
patterns to the LHS and RHS vectors. As coloring schemes change those
patterns, they may change the computational intensity, and we have to
investigate this effect in more detail. We apply the basic
\acrshort{MC} scheme generated by COLPACK~\cite{COLPACK} to
parallelize \acrshort{SymmSpMV} and compare it with \acrshort{SpMV},
which serves as our performance yardstick. Note that any required
preprocessing is excluded from the timings. In~\Cref{fig:motivation}
we present performance and data transfer volumes for
the \emph{Spin-26} matrix on a single socket of the \IVB
and \SKX systems. 
For \acrshort{SpMV} we recover the memory bandwidth
saturation pattern as we fill the chip (\Cref{fig:motivation_symm_spmv,fig:motivation_symm_spmv_skx}).
 Measuring the actual data volume from main memory using \LIKWID
we find $16.24$ and $16.36$ \BYTE per nonzero matrix entry (\Cref{fig:motivation_data,fig:motivation_data_skx})
on \IVB and \SKX architectures.
 This corresponds to the denominator of
$I_\mathrm{\acrshort{SpMV}}$ in~\Cref{eq:SpMV_intensity}, so we can
determine $\alpha_\mathrm{\acrshort{SpMV}}=0.351$ for \IVB and $0.367$ for \SKX, 
and we can calculate an 
optimistic bound for the intensity of \acrshort{SymmSpMV} according to ~\Cref{eq:SymmSpMV_intensity}.
 Using the copy and the load-only bandwidth of \IVB (see~\Cref{tab:test_bed}) 
 in \Cref{eq:upper_performance} we find a maximum
attainable {\acrshort{SymmSpMV}} performance range for this matrix of
$P_\mathrm{\acrshort{SymmSpMV}} = 7.63 \text{--} 8.96\,\GF$, 
while for \SKX we expect  
$P_\mathrm{\acrshort{SymmSpMV}} = 19.49 \text{--} 21.55\,\GF$ . 
This indicates a possible
speedup of approximately 1.4$\times$ -- 1.6$\times$ compared to the \acrshort{SpMV}
baseline ($5.5\,\GF$ and $13.41\,\GF$ on \IVB and \SKX), the \acrshort{SymmSpMV} implementation
using \acrshort{MC} falls short of this expectation and is more than
three times slower than \acrshort{SpMV}.
\begin{figure}[t]
  	\centering
    \subfloat[SymmSpMV]{\label{fig:motivation_symm_spmv}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out/motivation_symm_spmv}}
  	\subfloat[Data traffic]{\label{fig:motivation_data}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out/motivation_data_wo_RACE}} 
    \subfloat[SymmSpMV]{\label{fig:motivation_symm_spmv_skx}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out_skx/motivation_symm_spmv}}
    \subfloat[Data traffic]{\label{fig:motivation_data_skx}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out_skx/motivation_data_wo_RACE}}
  	\caption{Scaling performance of \acrshort{SymmSpMV} with \acrshort{MC} and \acrshort{ABMC} compared to \acrshort{SpMV} on one socket of \IVB and \SKX is shown in \Cref{fig:motivation_symm_spmv,fig:motivation_symm_spmv_skx} respectively. \Cref{fig:motivation_data,fig:motivation_data_skx} show average data traffic per nonzero entry ($\acrshort{nnz}$) of the full matrix as measured with \LIKWID for all cache levels and main memory on full socket of \IVB and \SKX respectively. The Spin-26 matrix was prepermuted with \acrshort{RCM}.}
  	\label{fig:motivation}
  \end{figure}
 
  \begin{figure}[t]
  	\centering
  	\includegraphics[scale=0.45]{pics/mc_alpha_problem/mc_alpha_unsymm}
  	\caption{Illustration of the increase of $\alpha$ by \acrshort{MC}. Numbers represent thread ids. Note that this figure shows only rows of the matrix permuted according to \acrshort{MC}, but in practice one would permute both rows and columns.}
  	\label{fig:mc_alpha}
  \end{figure}
  
The reason for this decrease is the nature of the \acrshort{MC}
permutation. For \DTWO coloring, sets of structurally orthogonal
rows have to be determined \cite{dist_k_def}, \ie rows that do not
overlap in any column entry. These sets are referred to as colors. \Cref{fig:mc_alpha} shows the corresponding permutation
and the obtained sets of colors when applied to a toy problem with high
data locality. Different rows of the same color can be executed in
parallel, but colors are operated one after the
other. After \acrshort{MC} a color may contain rows from very distant
parts of the matrix, potentially destroying data locality.  Assuming
that the \acrshort{LLC} can hold a maximum of six elements, we find
that the RHS vector must be loaded every time for each
new color increasing the data traffic from main memory.
This degradation of data
locality is the reason why we observe 3$\times$ more bytes
per nonzero for \acrshort{SymmSpMV} with \acrshort{MC} compared
to \acrshort{SpMV} for the \emph{Spin-26} matrix,
as seen in~\Cref{fig:motivation_data,fig:motivation_data_skx}.  However,
our performance model indicates that \acrshort{SymmSpMV} should
exhibit only 0.7$\times$ the data traffic of \acrshort{SpMV} (see
red dotted line in \Cref{fig:motivation_data,fig:motivation_data_skx}). Of course this
effect strongly depends on the matrix structure, the matrix size, and
the cache size.
        
\Acrshort{ABMC}~\cite{ABMC} tries to preserve data locality by first partitioning
the entire matrix into blocks of specified size and then
applying \acrshort{MC} to these blocks. Threads then work in parallel
between blocks of the same color. Along the lines of \cite{Park_HPCG} 
we use \METIS \cite{METIS} to partition the matrix into blocks, and COLPACK 
for \acrshort{MC}. The size of blocks for \acrshort{ABMC} is
determined by a parameter scan (range 4 \ldots 128;
see~\cite{ABMC})\@. As stated above, the timing for the performance measurements
excludes preprocessing and the parameter search. This method reduces the 
data traffic (see \Cref{fig:motivation_data,fig:motivation_data_skx}) as there is better data
locality within a block. Consequently, the performance improves
over plain \acrshort{MC} (see \Cref{fig:motivation_symm_spmv,fig:motivation_symm_spmv_skx}). However,
we are far from the performance model prediction. In
addition to data locality, other factors like global synchronizations
and false sharing also contribute to this. These
effects strongly depend on the number of colors and in general
increase with chromatic number. In the case of the Spin-26 matrix the overhead of
synchronization is roughly 10\% for the \acrshort{MC} method.  For most of
the matrices considered in this work one can also observe a strong
positive correlation between false sharing and the number of threads
for \acrshort{SymmSpMV} kernels due to the indirect writes
in \acrshort{SymmSpMV}.
 

