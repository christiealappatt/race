
This paper addresses the general problem of generating hardware-efficient \DK coloring
 of undirected graphs for modern multicore processors. As an application we 
 choose parallelization of the \acrshort{SymmSpMV} operation. We cover thread-level 
 parallelization and focus on a single multicore processor. The main contributions
  can be summarized as follows: 
\begin{itemize}
\item A new recursive algebraic coloring scheme (RACE) is proposed, 
which generates hardware-efficient \DK colorings of undirected graphs. 
Special emphasis in the design of RACE is put on achieving data locality, 
generating levels of parallelism matching the core count of the underlying 
multicore processor and load balancing for shared-memory parallelization.
\item We propose shared-memory parallelization of \acrshort{SymmSpMV}  
using a \DTWO coloring of the underlying undirected graph to avoid
 write conflicts and apply RACE for generating the colorings.
\item A comprehensive performance study of shared-memory parallel \acrshort{SymmSpMV} 
using RACE demonstrates the benefit of our approach. Performance modeling
 is deployed to substantiate our performance measurements, and a comparison to
  existing coloring methods as well a vendor optimized library (Intel MKL) 
  are presented. The broad applicability and the sustainability is validated 
  by using a wide set of 31 test matrices and two very different generations 
  of Intel Xeon processors.
\item We extend the existing proven \acrshort{SpMV} performance modeling approach
 to the \acrshort{SymmSpMV} kernel. 
In the course of the 
 performance analysis we further demonstrate why in some cases the ideal speedup
 may not be achievable.
\end{itemize}
We have implemented our graph coloring algorithms in the open source library \acrfull{RACE}.\footnote{\href{http://tiny.cc/RACElib}{http://tiny.cc/RACElib}}
Information required to reproduce the performance numbers provided in this 
paper is also available.\footnote{\href{http://tiny.cc/RACElib-AD}{http://tiny.cc/RACElib-AD}}


This paper is organized as follows: Our software and hardware environment as well as
 the benchmark matrices are introduced in \Cref{Sec:test_bed}. 
In \Cref{Sec:test_kernels} we describe the properties
of the \acrshort{SpMV} and \acrshort{SymmSpMV} kernels, including
\roofline performance limits, and motivate the need for an advanced coloring scheme.
In \Cref{Sec:race} we detail the steps of the \acrshort{RACE} algorithm
via an artificial stencil matrix and show how recursive level group construction
and coloring can be leveraged to exploit a desired level of parallelism
for \DK dependencies. The interaction between the parameters of the method
and their impact on the parallel efficiency is studied in \Cref{Sec:param_study}.
\Cref{Sec:expt} presents performance data for \acrshort{SymmSpMV}
for a wide range of matrices on two different multicore systems,
comparing \acrshort{RACE} with \acrshort{ABMC} and \acrshort{MC} as well as
Intel MKL, and also shows the efficiency of \acrshort{RACE} 
 with respect to the \roofline model.
\Cref{Sec:conclusion} concludes the paper and gives an outlook to
future work. 

