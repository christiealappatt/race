
The \acrshort{RACE} method has a set of input
parameters $\{\epsilon_s; s=0,1,\ldots\}$ which control the
assignment of threads to adjacent level groups.
To determine useful settings, we analyze the interaction
between these input parameters, the number of threads used, and the
parallel efficiency of the generated workload distribution.

As the internal tree structure contains all information about the
final workload distribution, we can use it to identify the critical
path in terms of workload and thus determine the parallel
efficiency. To this end we introduce the \effRow for every node
(or \levelGroup) $\acrshort{nrowsEff}(T_s(i))$, which is a measure for
the absolute runtime to calculate the
corresponding \levelGroup. For \levelGroups that are not further
refined (\ie the leaf nodes) this value is their actual workload, \ie the
number of rows assigned to them ($\acrshort{nrowsEff}(T_0(0)) = 15$
in \Cref{fig:rec_2d-7pt_tree}). For an inner node, the \effRow is the
sum of the maximum workload (\ie the maximum \effRow value) across each of
the two colors of its child nodes:
\begin{align*}
\acrshort{nrowsEff}(T_s(i)) &= \max\left(\acrshort{nrowsEff}(T_{s+1}(j) \subseteq T_s(i))\right) + \max\left(\acrshort{nrowsEff}(T_{s+1}(j+1) \subseteq T_s(i))\right)\\
 & \text{for } j=0,2,\ldots
\end{align*}
Such a definition is based on the idea that nodes at a given stage $s$
have to synchronize with each other and have to wait for their
siblings with the largest workload in each color. Propagating
this information upwards on the tree until we reach the root node
constructs the critical path in terms of longest runtime taking into
account single thread workloads, dependencies, and
synchronizations. Thus, the final value in the root node
$\acrshort{nrowsEff}(T_{-1}(0))$ can be considered as the effective
maximum workload of a single thread. Dividing the globally optimal
workload per thread,
${\acrshort{nrows}^\mathrm{total}}/{\acrshort{nthreads}}$, by
this number gives the parallel efficiency ($\eta$) of our workload
distribution:
\begin{align*}
	\eta &= \frac{ \acrshort{nrows}^\mathrm{total}} {\acrshort{nrowsEff}(T_{-1}(0)) \times \acrshort{nthreads}}. 
\end{align*}
For the tree presented in \Cref{fig:rec_2d-7pt_tree}, the parallel
efficiency is limited to $\eta=\frac{256}{44 \times 8 } = 0.73$ on
eight threads, \ie the maximum parallel speedup is $5.8$.

\subsection{Parameter analysis and selection}
\label{subsec:param_analysis}
The parallel efficiency \acrshort{eta} as defined above can be calculated for
any given matrix, number of threads $\acrshort{nthreads}$, and choice of
$\{\epsilon_s; s=0,1,\ldots\}$; it reflects the quality of parallelism generated
by \acrshort{RACE} for the problem at hand.  This way we can understand the
interaction between these parameters and identify useful choices for
the $\epsilon_s$. Of course, running a specific kernel such
as \acrshort{SymmSpMV} on actual hardware will add further hardware and software
constraints such as attainable memory bandwidth or cost of synchronization.

As a first step we can limit the parameter space by simple corner case
analysis. 
The smallest possible value of $\epsilon_s$ is the 
	maximum deviation of a real number from
	its nearest integer, which is $0.5$. On the other
hand, the largest possible value is one.
Setting all parameters ($\epsilon_s$) close to one requests high-quality load
balancing but may prevent our balancing scheme from terminating. 
In the extreme
case of $\{\epsilon_s=1; s=0,1,\ldots\}$ the scheme may generate only
two \levelGroups (one of each color) in each recursion, assign all threads to
them, and may further attempt to refine them in the same way.  
 A range of [$0.5$,$0.9$] for the
$\epsilon_s$ is therefore used in the following analysis. 
To ensure termination, the default $\epsilon_s$ value is 
	chosen to be 0.5\@. Thus, even if sufficient parallelism cannot
	be found at a user-specified $\epsilon_s$ value for the initial 
	stages of recursion, the algorithm will choose 
	the default $\epsilon_s = 0.5$ in later stages and eventually terminate.

For an initial discussion we have
selected the \emph{inline\_1} matrix (see \Cref{tab:test_mtx}), which has a
rather small amount of parallelism and thus represents an important
corner case. In \Cref{fig:inline_param_study} we demonstrate the impact of
different choices for $\epsilon_0$ and $\epsilon_1$ on the parallel efficiency
for thread counts up to 100, which is a useful limit for modern CPU-based
compute nodes.
\begin{figure}[t]
    \centering
    \subfloat[$\eta$ versus \acrshort{nthreads} for \emph{inline\_1} matrix, $\epsilon_1 = 0.5$ ]{\label{fig:inline-a}\includegraphics[width=0.45\textwidth , height=0.2\textheight]{pics/param_study/threads_vs_eff}}
    \hspace{1.5em}
    \subfloat[\acrshort{nthreads}=25]{\label{fig:inline-b}\includegraphics[width=0.45\textwidth , height=0.2\textheight]{pics/param_study/scaling_eps_1_25_threads}}

    \subfloat[\acrshort{nthreads}=45 ]{\label{fig:inline-c}\includegraphics[width=0.45\textwidth , height=0.2\textheight]{pics/param_study/scaling_eps_1_45_threads}}
    \hspace{1.5em}
    \subfloat[\acrshort{nthreads}=100 ]{\label{fig:inline-d}\includegraphics[width=0.45\textwidth , height=0.2\textheight]{pics/param_study/scaling_eps_1_100_threads}}
    \caption{Parameter study on the \emph{inline\_1} matrix. In \Cref{fig:inline-b,fig:inline-c,fig:inline-d} each of the lines in the plot are iso-$\epsilon_1$ and impact of $\eta$ with respect to $\epsilon_0$ is shown. $\epsilon_s$ for $s>1$ is fixed to $0.5$.}
	\label{fig:inline_param_study}
\end{figure}
For $s > 1$ we always set the default value of $\epsilon_s=0.5$. The limited
parallelism can be clearly observed in \Cref{fig:inline-a}, with efficiency
steadily decreasing with increasing thread count. At
$\epsilon_1=0.5$ there is only a minor impact of the parameter
$\epsilon_0$ for small thread counts ($\acrshort{nthreads}<30$). In \Cref{fig:inline-b,fig:inline-c,fig:inline-d} the interplay
between these two parameters is analyzed at different thread counts in more
detail. We find that up to intermediate parallelism ($\acrshort{nthreads}=50$)
the exact choice has only a minor impact on the parallel efficiency.
For larger parallelism the interplay becomes more intricate,
where too large values of $\epsilon_{0,1}$ may lead to stronger imbalance. Based
on this evaluation, we choose $\epsilon_{0,1}=0.8$ and $\epsilon_s=0.5$ for $s>1$
for all subsequent performance measurements. The quality of this choice in terms of
parallel efficiency for all matrices is presented
in \Cref{fig:param_all_mtx_stat}. Here we plot the $\eta$ value for all the
matrices over a large thread count. We find that our parameter setting achieves
parallel efficiencies of 75\% or higher for 75\% of the matrices up to an 
intermediate thread count of 40.
   \begin{figure}[t]
   	\centering
   	\includegraphics[width=0.9\textwidth]{pics/param_study/scatter_plot}
   	\caption{Parallel efficiency $\eta$ versus \acrshort{nthreads} for all test matrices with $\epsilon_{0,1} = 0.8$ and $\epsilon_{s>1} = 0.5$.}
  	\label{fig:param_all_mtx_stat}
   \end{figure}
Representing the upper (lower) values in \Cref{fig:param_all_mtx_stat} is the
best (worst) case matrix \emph{Graphene-4096} (\emph{crankseg\_1}), exhibiting
almost perfect (very low) parallel efficiency at intermediate to high thread
counts.

Finally, we evaluate the scalability of RACE using these two corner cases and
the \emph{inline\_1} matrix as well as the \emph{parabolic\_fem} matrix.
The latter is small enough to fit into the cache.  In \Cref{fig:corner_cases_param} we
mimic scaling tests on one Skylake processor with up to 20 cores (\ie
threads) and plot the parallel efficiency $\eta$ as well as the maximum number
of threads which can be ``perfectly'' used \acrshort{threadEff} (\ie
$\acrshort{threadEff} = \eta\times\acrshort{nthreads}$).  The unfavorable structure
of the \emph{crankseg\_1} matrix puts strict limits on parallelism even for low
thread counts.  The combination of small matrix size with a rather dense
population (see \Cref{table:bench_matrices}) leads to large inner levels when
constructing the graph, triggering strong load imbalance when using more than six threads.
A search for better $\epsilon_s$ slightly changes the 
scaling characteristic but not the maximum parallelism that can be extracted. 
For the \emph{inline\_1} matrix we find a weak but steady decrease of the parallel
efficiency, which is in good agreement with the discussion
of \Cref{fig:inline_param_study}. The other two matrices scale very well in the
range of thread counts considered.


The corresponding performance measurements for the \acrshort{SymmSpMV} kernel
(see \Cref{sect:SymmSpmv}) on a single \SKX processor chip with 20 cores are
shown in \Cref{fig:corner_cases_scaling}.\footnote{For the benchmarking setup
see \Cref{Sec:expt}.}
For the \emph{crankseg\_1} matrix (see \Cref{fig:crankseg_scaling}) we recover
the limited scaling due to load imbalance as theoretically predicted. A
performance maximum is at nine cores, where the maximum \acrshort{SpMV}
performance can be slightly exceeded. However, based on the \roofline performance
model given by \Cref{eq:SymmSpMV_intensity,eq:upper_performance} together with
the matrix parameters from \Cref{table:bench_matrices}, a theoretical speedup of
approximately two as compared to \acrshort{SpMV} can be expected for the full
processor chip under best conditions.
Indeed, in case of the \emph{inline\_1} and \emph{Graphene-4096}
matrices, performance
scales almost linearly until the main memory bandwidth bottleneck is hit. The
saturated performance is in good agreement with the \roofline limits.
Note that even though the \emph{inline\_1} matrix does not exhibit
perfect theoretical efficiency ($\eta$ $\approx 0.85$ at
$\acrshort{nthreads}=20$), it still generates sufficient parallelism to achieve
main memory saturation: the memory bottleneck can mitigate
a limited load imbalance.

The peculiar performance behavior of
\emph{parabolic\_fem}
(see \Cref{fig:parabolic_fem_param,fig:parabolic_fem_scaling}) is due to 
its smallness ($\approx 23$ \MB), which lets it fit into the caches of the
Skylake processor (\acrshort{LLC} size $= 28$ \MB). Thus, performance is not
limited by the main memory bandwidth constraint and the \roofline model limits do
not apply.
\begin{figure}[t]
	\centering
	\subfloat[\emph{crankseg\_1}]{\label{fig:crankseg_param}\includegraphics[width=0.24\textwidth]{pics/param_study/corner_cases/crankseg_1}}
	\subfloat[\emph{inline\_1}]{\label{fig:inline_param}\includegraphics[width=0.24\textwidth]{pics/param_study/corner_cases/inline_1}}	
	\subfloat[\emph{parabolic\_fem}]{\label{fig:parabolic_fem_param}\includegraphics[width=0.24\textwidth]{pics/param_study/corner_cases/parabolic_fem}}
	\subfloat[\emph{Graphene-4096}]{\label{fig:Graphene-4096_param}\includegraphics[width=0.24\textwidth]{pics/param_study/corner_cases/Graphene-4096}}	
	\caption{\acrshort{threadEff} and $\eta$ versus \acrshort{nthreads} for
	the four corner case matrices, with the same settings used in experiment
	runs. \acrshort{threadEff} is defined as
	$\eta\times\acrshort{nthreads}$.}
	\label{fig:corner_cases_param}
\end{figure}
\begin{figure}[t]
	\centering
	\subfloat[\emph{crankseg\_1}]{\label{fig:crankseg_scaling}\includegraphics[width=0.23\textwidth , height=0.18\textheight]{pics/results/skx/corner_cases_scaling/plots/RCM/crankseg_1_RCM}}
	\subfloat[\emph{inline\_1}]{\label{fig:inline_scaling}\includegraphics[width=0.23\textwidth , height=0.18\textheight]{pics/results/skx/corner_cases_scaling/plots/RCM/inline_1_RCM}}	
	\subfloat[\emph{parabolic\_fem}]{\label{fig:parabolic_fem_scaling}\includegraphics[width=0.23\textwidth , height=0.18\textheight]{pics/results/skx/corner_cases_scaling/plots/RCM/parabolic_fem_RCM}}
	\subfloat[\emph{Graphene-4096}]{\label{fig:Graphene-4096_scaling} \includegraphics[width=0.23\textwidth , height=0.18\textheight]{pics/results/skx/corner_cases_scaling/plots/RCM/Graphene-4096_RCM}}	
	\caption{Parallel performance measurements of \acrshort{SymmSpMV}
	with \acrshort{RACE} on one \SKX socket for the four
	corner case matrices. The performance of the
	basic \acrshort{SpMV} kernel is presented for reference. For the
	matrices \Cref{fig:inline_scaling,fig:parabolic_fem_scaling,fig:Graphene-4096_scaling}
	the maximum \roofline performance limits \Cref{eq:upper_performance} are
	given using the computational intensity \Cref{eq:SymmSpMV_intensity} for
	the two extreme cases of load-only memory bandwidth (RLM-load) and copy
	memory bandwidth (RLM-copy). The measured full socket main memory data
	traffic per nonzero entry of the symmetric matrix (in \BYTE) for the \acrshort{SymmSpMV}
	 operation is also shown, where values below 12 \BYTE indicate caching of the matrix entries.}
	\label{fig:corner_cases_scaling}
\end{figure}

We have demonstrated that a simple choice for the only set of RACE input
parameters $\{\epsilon_s; s=0,1,\ldots\}$ can extract sufficient parallelism for
most matrices considered in this study. Moreover, the parallel efficiency as
calculated by RACE in combination with the \roofline performance model is a good
indication for scalability and maximum performance of the actual computations.
