
We evaluate the performance of the \acrshort{SymmSpMV} based on parallelization and reordering performed by \acrshort{RACE} and compare it with the two MC approaches introduced above and the \acrshort{MKL}. 
As a measure of baseline performance we choose the general \acrshort{SpMV} kernel and use the performance model introduced in \Cref{Sec:test_kernels} to quantify the quality of our absolute performance numbers.
As the deviations between different measurement runs are less than 5\%, we do  not show the 
error bar in our performance measurements.

\subsection{Experimental Setup}

All matrix data are encoded in the CRS format. For the \acrshort{SymmSpMV}  only the nonzeros of the upper triangular matrix are stored. In the case of RACE and the coloring approaches every thread executes the \acrshort{SymmSpMV} kernel \Cref{alg:SymmSpMV} with appropriate outer loop boundary settings depending on the color (MC, ABMC) or \levelGroups (\acrshort{RACE}) to be computed. 
Balancing by number of non-zeros (\acrshort{nnz}) is used for the load balancing step of \acrshort{RACE} in all performance measurements.
\Inorder to ensure vectorization of the inner loop in \Cref{alg:SymmSpMV} we use the SIMD pragma \texttt{\#pragma simd reduction(+:tmp) vectorlength(VECWIDTH)}. Here \texttt{VECWIDTH} is the maximum vector width supported by the architecture, \ie \texttt{VECWIDTH = 4 (8)} for \IVB (\SKX). 

The  \acrshort{MKL} offers two choices for the two sparse matrix kernels under consideration: First, CRS based data structures are provided and are used in the subroutines (\texttt{mkl\_cspblas\_dcsrgemv} for \acrshort{SpMV}  and  \texttt{mkl\_cspblas\_dcsrsymv} for \acrshort{SymmSpMV}) without any modification (MKL). This mode of operation is deprecated from \acrshort{MKL}.v.18 update 2. Instead, the inspector-executor mode (MKL-IE) is recommended to be used. Here, the user initially provides the matrix along with hints (\eg symmetry) and operations to be carried out to the inspector routine (\texttt{mkl\_sparse\_set\_mv\_hint}). Then an optimization routine (\texttt{mkl\_sparse\_optimize}) is called where the matrix is  preprocessed based on the inspector information to achieve best performance and highest parallelism for the problem at hand. The subroutine \texttt{mkl\_sparse\_d\_mv} is then used to do the \acrshort{SpMV} or \acrshort{SymmSpMV} operations on this optimized matrix structure. This approach does not provide any insight into which kernel or data structure is actually used ``under the hood.''

In the performance measurements the kernels are executed multiple times \inorder to ensure reasonable measurement times and average out potential performance fluctuations. 
We use two ring buffers (at least of 50 MB each) holding separate vectors of size \acrshort{nrows} in order to avoid vector data reuse across kernel executions.\footnote{
Depending on the algorithm that the kernels are embedded in, its implementation, and the parameters of the matrix,
cache reuse may occur in practice.
 For a quantification of this effect, one can use the performance models (\ref{eq:SpMV_intensity}) and (\ref{eq:SymmSpMV_intensity}).}
 After each kernel invocation we switch to the next vector in the two buffers. 
 We run over these two buffers 100 iterations (times) and report the mean performance.

For all methods and libraries the input matrices have been preprocessed with \acrshort{RCM} bandwidth reduction using the \SPMP library \cite{SpMP}. This provides the same or better performance on all matrices as compared to the original ordering. If not otherwise noted we use the full processor chip and assign one thread to each core. As we focus on a single chip and \acrfull{SNC} is not enabled on \SKX, no NUMA data placement effects impact our results.  
Simultaneous multi-threading (SMT) is not used since for most of the matrices in \Cref{tab:test_mtx}, the processor can saturate the main memory bandwidth with one thread per core.



\subsection{Results}
Before we evaluate the performance across the full set of matrices presented in \Cref{table:bench_matrices} we return to the analysis of the \acrshort{SymmSpMV} performance and data traffic for the Spin-26 matrix  which we have presented in \Cref{Sec:motivation} for the established coloring approaches. 
\subsubsection{Analysis of \acrshort{SymmSpMV} kernel using RACE for the Spin-26 matrix}
\label{Sec:Spin26full}
The shortcomings in terms of performance and excessive data transfer for parallelization of \acrshort{SymmSpMV} using MC and ABMC have been demonstrated in \Cref{fig:motivation}. We extend this evaluation by comparison with the \acrshort{RACE} results in \Cref{fig:motivation_w_RACE}.
 \begin{figure}[t]
 	\centering
 	\subfloat[SymmSpMV]{\label{fig:motivation_symm_spmv_w_RACE}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out/motivation_symm_spmv_w_RACE}}
 	\subfloat[Data traffic]{\label{fig:motivation_data_w_RACE}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out/motivation_data_w_RACE}}
  	\subfloat[SymmSpMV]{\label{fig:motivation_symm_spmv_w_RACE_skx}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out_skx/motivation_symm_spmv_w_RACE}}
  	\subfloat[Data traffic]{\label{fig:motivation_data_w_RACE_skx}\includegraphics[width=0.248\textwidth, height=0.20\textheight]{pics/motivation/out_skx/motivation_data_w_RACE}}
 	\caption{Performance (\Cref{fig:motivation_symm_spmv_w_RACE}) and data traffic (\Cref{fig:motivation_data_w_RACE}) analysis for \acrshort{SymmSpMV} kernel with Spin-26 matrix using \acrshort{MC}, \acrshort{ABMC}, and \acrshort{RACE} on a single socket of \IVB. The corresponding measurements for a single socket of \SKX is shown in \Cref{fig:motivation_symm_spmv_w_RACE_skx,fig:motivation_data_w_RACE_skx}. The roofline performance model (using copy and load-only bandwidth) and the performance of the \acrshort{SpMV} kernel is plotted for reference in scaling plots \Cref{fig:motivation_symm_spmv_w_RACE,fig:motivation_symm_spmv_w_RACE_skx}. The average data traffic per nonzero entry ($\acrshort{NNZR}$) of the full matrix as measured with \LIKWID for all cache levels and main memory is shown together with the minimal value for main memory access (horizontal dashed line) in \Cref{fig:motivation_data_w_RACE,fig:motivation_data_w_RACE_skx}.}
 	\label{fig:motivation_w_RACE}
 \end{figure}
The figures clearly demonstrate the ability of \acrshort{RACE} to ensure high data locality 
in the parallel \acrshort{SymmSpMV} kernel. The actual main memory traffic achieved is  
in line with the minimum traffic for that matrix (see discussion in \Cref{Sec:motivation}) 
and a factor of up to 4$\times$ lower than the coloring approaches. 
Correspondingly, \acrshort{RACE} \acrshort{SymmSpMV} performance is at least $3.3\times$ 
higher than its best competitor and $25\%$ better than the \acrshort{SpMV} kernel on both 
architectures. It achieves more than 84\% of the roofline performance limit based on 
the copy main memory performance. Note that the indirect update of the LHS vector 
will generate a store instruction for every inner loop iteration (see \Cref{alg:SymmSpMV}), 
while the \acrshort{SpMV} kernel only does a final store at the end of the inner loop iteration. 
In combination with the low number of nonzeros per row (\acrshort{NNZR}) of the Spin-26 matrix, 
the ``copy'' induced limit poses a realistic upper performance bound.  
\subsubsection{Analyzing absolute performance of RACE}
We now extend our \acrshort{RACE}  performance investigation to the full set of test matrices presented in~\Cref{table:bench_matrices}. In \Cref{fig:spmv_vs_symm_spmv_ivy,fig:spmv_vs_symm_spmv_skx} the performance results for the full \IVB processor chip (10 cores) and the full \SKX processor chip (20 cores) are presented along with the upper roofline limits and the performance of the baseline \acrshort{SpMV} kernel using \acrshort{MKL}.  
 \begin{figure}[t]
	\centering
	\subfloat[\IVB]{\label{fig:spmv_vs_symm_spmv_ivy}\includegraphics[width=0.95\textwidth, height=0.34\textheight]{pics/results/ivy/data_symm_spmv/plot_generator/perf_vs_mtx_RACE_w_SpMV_w_MKL/perf}}
	\hspace{1em}
	\subfloat[\SKX]{\label{fig:spmv_vs_symm_spmv_skx}\includegraphics[width=0.95\textwidth, height=0.34\textheight]{pics/results/skx/data_symm_spmv/plot_generator/perf_vs_mtx_RACE_w_SpMV_w_MKL/perf}}
	\caption{Performance of \acrshort{SymmSpMV} executed with \acrshort{RACE} compared to the performance model and \acrshort{MKL} implementations. \acrshort{SpMV} performance obtained using \acrshort{MKL} library is also shown for reference. The model prediction is derived for  bandwidths in the range of load and copy bandwidth, and using the measured $\alpha_{\acrshort{SpMV}}$ shown in \Cref{table:alpha_values}.}
	\label{fig:SpMV_vs_SymmSpMV}
\end{figure}
The matrices are arranged along the abscissa according to the ascending number of rows (\acrshort{nrows}), \ie increasing size of the two vectors involved in \acrshort{SymmSpMV}. Overall \acrshort{RACE}  performance comes close to or matches our performance model for many test cases on both architectures. 
A comparison of the architectures shows that the corner case matrices \texttt{crankseg\_1} and \texttt{parabolic\_fem} have a strikingly different behavior for \acrshort{RACE}. For \texttt{crankseg\_1} this is caused by the limited amount of parallelism in its structures. Here we refer to the discussion of \Cref{fig:crankseg_param} where best performance and highest parallelism (\acrshort{threadEff}) were achieved at approximately 10 cores. Using only 9 cores on \SKX lifts the \acrshort{SymmSpMV} performance of  \texttt{crankseg\_1} slightly above the \acrshort{SpMV} level of the MKL. The \texttt{parabolic\_fem} has been chosen to fit into the \acrshort{LLC} of the \SKX architecture to provide a corner case where scalability is not intrinsically limited by main memory bandwidth (see  \Cref{fig:parabolic_fem_param}) and thus our roofline performance limit does not apply for this matrix on \SKX. However, on \IVB the matrix data set just exceeds the LLC and the performance is in line with our model. 

On both architectures a characteristic drop in performance levels is encountered around the \texttt{Flan\_1565} and \texttt{G3\_circuit} matrices, where the aggregate size of the two vectors (25 MB) approaches the available LLC sizes. For smaller matrices we have a higher chance that the vectors stay in the cache during the \acrshort{SymmSpMV}, \ie the vectors must only be transferred once between main memory and the processor for every kernel invocation. 
For larger matrices (\ie larger $N_r$) the reuse of vector data during a single \acrshort{SymmSpMV} kernel decreases and vector entries may be accessed several times from the main memory. This is reflected by the increase in measured $\alpha_{\acrshort{SpMV}}$ (assumed $\alpha_{\acrshort{SymmSpMV}}$) values for matrices with index 20 and higher in \Cref{table:alpha_values}. 

In short, \acrshort{RACE} has an average speedup of 1.4$\times$ and 1.5$\times$ compared to \acrshort{SpMV} on the \SKX and \IVB architectures, respectively. On \SKX \acrshort{RACE} \acrshort{SymmSpMV} attains on an average 87\% and 80\% of the roofline performance limits predicted using the copy and load bandwidth, respectively, while on \IVB we are 91\% and 83\% close to the respective performance models.

The MKL implementations of \acrshort{SymmSpMV} deserves a special consideration in this context.
Therefore,  in \Cref{fig:SpMV_vs_SymmSpMV} we also compare  our approach with the two \acrshort{MKL} options described above. For the MKL-IE variant we specify exploiting the symmetry of the matrix when calling the inspector routine. 
On the \IVB architecture, \acrshort{RACE} always provides superior performance levels and the best performing Intel variant depends on the underlying matrix. On the \SKX, however, MKL-IE always outperforms the deprecated MKL routine and is superior to \acrshort{RACE} for two matrices (\texttt{crankseg-1,offshore}). These are the same matrices where \acrshort{RACE} is slower than the MKL \acrshort{SpMV} kernel (see \Cref{fig:spmv_vs_symm_spmv_skx}). It can be clearly seen that the MKL-IE data for \acrshort{SymmSpMV} 
 are identical with the MKL \acrshort{SpMV} numbers presented in \Cref{fig:SpMV_vs_SymmSpMV}, \ie the inspector calls the baseline \acrshort{SpMV} kernel and uses the full matrix, though it knows about the symmetry of the matrix. One reason for that strategy might be that the parallelization approach used in the deprecated MKL implementation for \acrshort{SymmSpMV} is not scalable which would explain the fact that MKL is worse than MKL-IE for all cases on \SKX.  As neither the algorithm used to parallelize the \acrshort{SymmSpMV} nor its low level code implementation is known, we refrain from a deep analysis of the Intel performance behavior. 
In summary we find that \acrshort{RACE} is on average 1.4$\times$ faster than the best Intel variant and can achieve speedups of up to 2$\times$. 
Note that on \SKX the best MKL variant is always MKL-IE, 
	which has almost twice the memory footprint 
	compared to the  \acrshort{SymmSpMV} with \acrshort{RACE}.

\subsubsection{Single core performance}
Although single core performance is often considered not to be crucial for 
the full chip \acrshort{SymmSpMV} performance, we demonstrate that it is
vital to explain some of the performance behaviors. For example the drop in 
\acrshort{SymmSpMV} performance
 for matrices like  \texttt{Hubbard-12} and \texttt{delaunay\_n24} strongly correlates with
 the lower performance of the baseline \acrshort{SpMV} (see \Cref{fig:SpMV_vs_SymmSpMV}).
 These matrices are characterized by a rather low $\acrshort{NNZR}$ and a 
 larger $\alpha_{\acrshort{SpMV}}$ value. 
 The  $\alpha_{\acrshort{SpMV}}$ (= assumed $\alpha_{\acrshort{SymmSpMV}}$)
 measured for the \acrshort{SpMV}  kernel mainly accounts for the RHS vector traffic 
  and the actual $\alpha_{\acrshort{SymmSpMV}}$ may even be higher as \acrshort{SymmSpMV}
  requires two vectors to stay in cache concurrently. 
 Moreover, for these matrices the inner loop lengths are typically very short (approximately $\acrshort{NNZR}/2$ on average) and consequently the SIMD vectorization performed by the 
 compiler may become inefficient. This leads to lower single core performance
  as shown in \Cref{fig:SpMV_vs_SymmSpMV_single_core} for the \SKX architecture, 
  where bad performance of \acrshort{SymmSpMV}  and \acrshort{SpMV} can often be 
  correlated with a small \acrshort{NNZR} value. 
 \begin{figure}[t]
 	\centering
 	\includegraphics[width=0.95\textwidth, height=0.34\textheight]{pics/results/skx/data_symm_spmv_single_core/plot_generator/perf_vs_mtx_RACE_vs_MKL/perf}
 	\caption{Single core performance of \acrshort{SymmSpMV} on \SKX 
 	executed with \acrshort{RACE} compared to \acrshort{SpMV} performance using \acrshort{MKL}.}
 	\label{fig:SpMV_vs_SymmSpMV_single_core}
 \end{figure}
 %
 For several matrices these combined effects overcompensate the reduced matrix data traffic of the \acrshort{SymmSpMV}, leading to worse single core performance than running \acrshort{SpMV} with the full matrix. Using the \texttt{delaunay\_n24} matrix as a representative for this class of matrices we demonstrate the basic challenge for \acrshort{SymmSpMV} to exploit its basic performance advantage over \acrshort{SpMV} in \Cref{fig:scaling_delaunay}.
Starting with an approximately 25\% lower single core performance (0.75 GF/s versus 0.98 GF/s) but having a 50\% higher roofline performance limit (approximately 18 GF/s; see \Cref{fig:spmv_vs_symm_spmv_skx}) than the \acrshort{SpMV}, the \acrshort{SymmSpMV} is not able to saturate the main memory bandwidth of the \SKX on its 20 cores. As speculated above, the single core performance is limited by inefficient SIMD vectorization of the extremely short inner loop and switching back to scalar code does improve performance by 15\% (see \Cref{fig:scaling_delaunay}).\footnote{A SIMD-friendly data layout \cite{Moritz_sell,Yzelman_SIMD} and/or effective SIMD instructions \cite{Yzelman_SIMD} may improve the performance here. This is left to future work.} As we are still substantially off the bandwidth limit we see this benefit over the full chip. Using chips with larger core counts would allow for further improving the \acrshort{SymmSpMV} performance of this matrix. The same arguments hold for the \texttt{offshore} matrix but here the effect compared to \acrshort{SpMV} performance is even more pronounced on \SKX.  Here the full matrix can at least partially be held in the large aggregate cache between successive kernel invocations and its performance is not limited by the main memory bandwidth. In terms of caching effects we have also further identified at least partial caching of the matrix for \texttt{ship-003} and \texttt{pwtk} test cases by analyzing the overall data traffic in the kernel invocations. This is in line with their higher performance levels presented in \Cref{fig:spmv_vs_symm_spmv_skx}. 
  \begin{figure}[t]
  	\centering
  	\begin{minipage}[c]{0.4\textwidth}
  		\includegraphics[width=\textwidth]{pics/results/scaling_skx/plots/delaunay_n24}
  	\end{minipage}\hfill
  	\begin{minipage}[c]{0.55\textwidth}
  		\caption{Parallel performance  of \acrshort{SymmSpMV} (with \acrshort{RACE})  and \acrshort{SpMV} (with \acrshort{MKL}) for the \texttt{delaunay\_n24} matrix on one socket of \SKX. To disable vectorization (SymmSpMV-Scalar) we set \texttt{VECWIDTH = 1} when compiling the \acrshort{SymmSpMV} kernel.}
  		\label{fig:scaling_delaunay}
  	\end{minipage}
  \end{figure}
  %

\subsubsection{Comparing \acrshort{RACE} with \acrshort{MC} and \acrshort{ABMC}}
\begin{figure}[t]
	\centering
	\subfloat[\IVB]{\label{fig:symm_spmv_ivy}\includegraphics[width=0.95\textwidth, height=0.34\textheight]{pics/results/ivy/data_symm_spmv/plot_generator/perf_vs_mtx_RACE_vs_MC_ABMC/perf}}
	\hspace{1em}
	\subfloat[\SKX]{\label{fig:symm_spmv_skx}\includegraphics[width=0.95\textwidth, height=0.34\textheight]{pics/results/skx/data_symm_spmv/plot_generator/perf_vs_mtx_RACE_vs_MC_ABMC/perf}}
	\caption{Comparison of \acrshort{SymmSpMV} performance between \acrshort{RACE} and coloring variants \acrshort{MC} and \acrshort{ABMC}. Matrices are arranged in increasing number of rows (\acrshort{nrows}).}
	\label{fig:symm_spmv}
\end{figure}
Having well understood the performance characteristics of  \acrshort{SymmSpMV} with \acrshort{RACE} we finally compare this with the performance achieved by the two coloring methods in \Cref{fig:symm_spmv}. Here the underlying algorithm as well as implementation are known and are closely related to our approach. 
Overall the \acrshort{MC} is not competitive and provides low performance levels for almost all the matrices on both architectures. The \acrshort{ABMC} shows similar performance characteristics as \acrshort{RACE} until the two vectors involved in \acrshort{SymmSpMV} approach the size of the caches (cf. discussion of \Cref{fig:SpMV_vs_SymmSpMV}). For matrices with sufficiently small $N_r$ (left in the diagram) the method can achieve between 70\% and 90\% of \acrshort{RACE} performance on most cases. For matrices in the right part of the diagram with their higher \acrshort{nrows} and $\alpha_{\acrshort{SpMV}}$ values, the \acrshort{ABMC} falls substantially behind \acrshort{RACE}. Here, the strict orientation of the \acrshort{RACE} design towards data locality in the vector accesses delivers its full power. See also the data transfer discussion in \Cref{Sec:Spin26full} for the \texttt{Spin-26} matrix.
In total there are only three cases where \acrshort{ABMC} performance is on a par with or slightly above the \acrshort{RACE} measurement and the average speedup of \acrshort{RACE} is $1.5\times$ and $1.65 \times$ for \IVB and \SKX, respectively.
Note that all three methods use the same baseline kernels and thus performance differences between the methods do not arise from different low level code but from the ability to generate appropriate degrees of parallelism and to maintain data locality.

