Id,	 Matrix,			 NNZR,	 Measured alpha
0,	Anderson-16.5.mtx             ,	7,	0.318715
1,	audikw_1.mtx                  ,	82.284898,	0.063762
2,	bone010.mtx                   ,	72.632114,	0.052338
3,	channel-500x100x100-b050.mtx  ,	18.776498,	0.133898
4,	crankseg_1.mtx                ,	201.011476,	0.017876
5,	delaunay_n24.mtx              ,	5.999994,	0.319197
6,	dielFilterV3real.mtx          ,	80.979395,	0.067509
7,	Emilia_923.mtx                ,	44.419464,	0.085462
8,	F1.mtx                        ,	78.062291,	0.043622
9,	Fault_639.mtx                 ,	44.794105,	0.086085
10,	Flan_1565.mtx                 ,	75.029713,	0.052516
11,	FreeBosonChain-18.mtx         ,	12.461538,	0.262774
12,	FreeFermionChain-26.mtx       ,	13.52,	0.397282
13,	G3_circuit.mtx                ,	4.831872,	0.335974
14,	Geo_1438.mtx                  ,	43.921034,	0.091725
15,	Graphene-4096.mtx             ,	12.994629,	0.127774
16,	gsm_106857.mtx                ,	36.914194,	0.094584
17,	Hook_1498.mtx                 ,	40.665227,	0.094818
18,	HPCG-192.mtx                  ,	26.719725,	0.139089
19,	Hubbard-12.mtx                ,	12.998918,	0.231786
20,	Hubbard-14.mtx                ,	14.999709,	0.359807
21,	inline_1.mtx                  ,	73.090063,	0.034046
22,	nlpkkt120.mtx                 ,	27.339033,	0.165642
23,	nlpkkt200.mtx                 ,	27.6001,	0.172028
24,	offshore.mtx                  ,	16.331226,	0.105831
25,	parabolic_fem.mtx             ,	6.988304,	0.224973
26,	pwtk.mtx                      ,	53.389,	0.038276
27,	Serena.mtx                    ,	46.380672,	0.115621
28,	ship_003.mtx                  ,	66.427067,	0.039038
29,	Spin-26.mtx                   ,	14,	0.351781
30,	thermal2.mtx                  ,	6.98697,	0.227709
