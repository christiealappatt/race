COL_MTX_NAMES = 2
COL_PERF_1 = 6 6 6 6 6
COL_PERF_2 = 8 8 8 8 8
OP = /
PERF_FILES = ../../RACE/result.txt ../../ABMC/result.txt ../../MC/result.txt 
COLORS = orange cyan green yellow
LEGEND = RACE ABMC MC 
TEMPLATE = template.tex
GENERATED = derived_perf.tex
YLABEL = Scaled inverse runtime ($s^{-1}$)
TYPE = LINE
Y_SCALE = 1000
TITLE = 
#Symm-KACZ comparison on 1 socket of Intel Ivy Bridge


