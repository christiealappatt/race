COL_MTX_NAMES = 2
COL_PERF_1 = 5 3
PERF_FILES = ../../RACE/result.txt ../../MKL/result.txt 
COLORS =  orange magenta
LEGEND = RACE-SymmSpMV MKL-SpMV
MARK = default triangle*
TEMPLATE = template.tex
GENERATED = perf.tex
YLABEL = Perf (GF/s)
TYPE = LINE
TITLE = 
#SymmSpMV comparison on 1 socket of Intel Skylake


