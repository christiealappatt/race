% SIAM Shared Information Template
% This is information that is shared between the main document and any
% supplement. If no supplement is required, then this information can
% be included directly in the main document.
\begin{abstract}

The \acrfull{SymmSpMV} is an important building block for many
numerical linear algebra kernel operations or graph traversal
applications. Parallelizing \acrshort{SymmSpMV} on today's multicore
platforms with up to 100 cores is difficult due to the need to manage
conflicting updates on the result vector. Coloring approaches can be
used to solve this problem without data duplication, but existing
coloring algorithms do not take load balancing and deep memory
hierarchies into account, hampering scalability and full-chip
performance. In this work, we propose the \acrfull{RACE}, a novel
coloring algorithm and open-source library implementation, which
eliminates the shortcomings of previous coloring methods in
terms of hardware efficiency and parallelization overhead. We describe
the level construction, \DK coloring, and load balancing steps in
\acrshort{RACE}, use it to parallelize \acrshort{SymmSpMV}, and
compare its performance on 31 sparse matrices with other
state-of-the-art coloring techniques and Intel MKL on two modern
multicore processors.  \acrshort{RACE} outperforms all other
approaches substantially and behaves in accordance with the \roofline
model. Outliers are discussed and analyzed in detail.
While we focus on \acrshort{SymmSpMV} in this paper, 
our algorithm and software is applicable to any sparse matrix operation 
with data dependencies that can be resolved by distance-k coloring.



%
%\sout{Most existing
%  frameworks introduce a coloring phase to partition the part of the
%  vector into a group of color sets so that components within a set
%  are independent and can be processed simultaneously.} However,
%almost all coloring algorithms and software frameworks do not take
%advantage of both load balancing as well as the existing deep memory
%hierarchy and the characteristics of today's compute nodes to achieve
%improved node-level performance at full scale.  To address this, we
%propose in this paper a novel recursive algebraic coloring algorithm,
%and describe the software approach and the open source library
%\acrfull{RACE} which eliminates the shortcomings of previously
%existing coloring methods in terms of hardware efficiency and
%parallelization overhead. We first detail our algorithmic abstraction
%layer and the recursive higher order distance coloring in
%\acrshort{RACE} to accomplish code and performance portability on
%various platforms.  A comparison with other popular coloring
%techniques and a state-of-the-art library supplied by the Intel
%vendor, using 31 sparse matrices on the latest Intel processors, shows
%that the proposed approach in \acrshort{RACE} obtains a speedup of
%over 2.5 for \acrshort{SymmSpMV} compared to current state-of-the-art
%coloring algorithm.

\end{abstract}

\begin{comment}
Many iterative numerical methods for sparse systems and important building blocks of sparse linear algebra feature strong data dependencies. These may be loop-carried dependencies as they occur in many iterative solvers or preconditioners (\eg of Gauss-Seidel (\acrshort{GS}) type) or write conflicts as they show up in the parallelization of building blocks such as \acrfull{SymmSpMV} or \acrfull{SpMTV}. Scalable, hardware-efficient parallelization of such kernels is known to be a hard problem in the general case of sparse matrices which do not have simple regular structures.

A standard approach to solve this problem is \acrfull{MC} of the underlying matrix according to the requirements (\eg distance-1 for GS type iterations) of the algorithm. For irregular and/or large matrices this method may lead to load imbalance, frequent global synchronization and loss of data locality, which will reduce the single-node performance. These problems typically become more severe for higher order distance colorings and larger matrices. Among several improvement methods \acrfull{ABMC} is the most promising for addressing those problems. However, \acrshort{ABMC} has been applied only for distance-1 coloring problems. These methods have in common that they do not take into account the requirements of modern hardware in terms of data locality, load balancing and degrees of parallelism available in modern compute nodes.

Our method addresses matrices with symmetric structure (but not necessarily symmetric matrix entries) and thus can be represented by an undirected graph. In a first step we do a \acrshort{BFS} pre-processing for bandwidth reduction of the graph, which aims to increase data locality for the underlying sparse matrix problems: Starting from a root vertex we construct the $levels$ of the \acrshort{BFS} algorithm \ie \level $i$ consists of all nodes having distance $i$ to the root vertex. We then permute the graph such that vertex numbering increases with distance from the root vertex. Coloring the resulting \levels would be a naive approach to generate a \DK coloring but would for obvious reasons (\eg \level 0 contains only one vertex) often lead to severe load imbalance. Thus we perform in a second step $level$ $aggregation$ of neighboring \levels, which aims at conserving data locality. The choice of the size of each \levelGroup is subject to two major constraints: First, for a \DK coloring of the original graph/problem at least $k$ \levels are aggregated into a \levelGroup (\aka $supernode$). This means that alternate \levelGroups can be executed in parallel which is equivalent to a \DONE coloring of the \levelGroups. Second, we apply a criterion for load balancing that considers the total amount of hardware threads to be used at execution time and tries to balance workload across these threads evenly. At this stage it might happen that most of the vertices end up in a few \levelGroups. Therefore, depending on the size of the \levelGroups, different number of threads will be assigned to each of them. \Inorder to further parallelize within this \levelGroup for assigned threads the entire procedure is recursively repeated on their corresponding \subgraphs subject to the \DK constraint. The aggregation step is controlled by a single external parameter which influences the load imbalance introduced by forming each \levelGroup. Due to the recursive nature of this algorithm, nested parallelism is required. However, only local synchronization is required between the threads assigned to the same \subgraph.

We have implemented our algorithm in the open source library \acrfull{RACE}. \acrshort{RACE}  has two main usage scenarios: It can either return all relevant data structures and parallelization information required for manual implementation of the kernel at hand, or one can just use its callback function interface, which takes care of parallelization and all data handling automatically. As of now \acrshort{RACE} is limited to shared memory nodes as it only supports thread-level parallelism.\acrshort{RACE} uses the \acrfull{CRS} sparse matrix data format but can be easily extended to other formats.

Choosing a representative set of 28 sparse matrices we first perform an analysis of the impact of the aggregation parameter on the quality of the load balancing achieved for thread counts relevant for modern compute nodes. We observe that even for 60 threads, 70\% of the test matrices achieve more than 70\% of effective parallelism (theoretical efficiency) after accounting for load imbalances. This does not take into account scalability limitations like memory bandwidth bottleneck that occur in practice.

Finally we apply \acrshort{RACE} to two iterative schemes which require \DONE (\acrshort{GS} solver) and \DTWO (Kaczmarz solver) as well as for parallel symmetric sparse matrix vector multiplication (\acrshort{SymmSpMV})  (where \DTWO coloring is required to resolve write conflicts). We analyze \acrshort{RACE} performance and compare with \acrshort{MC} using \COLPACK, \acrshort{ABMC} using \COLPACK on top of \METIS,  \acrshort{MKL} if the kernel is available and data format (Recursive Sparse Blocks [RSB]) which is tailored for \acrshort{SymmSpMV}. Overall we achieve a speed-up of 2--2.5$\times$ compared to \acrshort{MC} and \acrshort{MKL} implementations. While we are on par with \acrshort{ABMC} for small matrices, for large matrices we gain almost a factor of 1.5--2$\times$. Comparisons with iterative kernels also highlight the convergence behavior of the method. Results show that the convergence of \acrshort{RACE} is better than \acrshort{MC} and is competitive with \acrshort{ABMC}.

\end{comment}

