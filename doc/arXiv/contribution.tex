% SIAM Shared Information Template
% This is information that is shared \acrshort{ABMC} the main document and any
% supplement. If no supplement is required, then this information can
% be included directly in the main document.


This paper addresses the general problem of generating hardware efficient \DK coloring
 of undirected graphs for modern multicore processors. As an application we 
 choose parallelization of the \acrshort{SymmSpMV} operation. We cover thread-level 
 parallelization and focus on a single multicore processor. The main contributions
  can be summarized as follows: 
\begin{itemize}
\item A new recursive algebraic coloring scheme (RACE) is proposed, 
which generates hardware efficient \DK colorings of undirected graphs. 
Special emphasis in the design of RACE is put on achieving data locality, 
generating levels of parallelism matching the core count of the underlying 
multicore processor and load balancing for shared memory parallelization.
\item We propose shared memory parallelization of \acrshort{SymmSpMV}  
using a \DTWO coloring of the underlying undirected graph to avoid
 write conflicts and apply RACE for generating the colorings.
\item A comprehensive performance study of shared memory parallel \acrshort{SymmSpMV} 
using RACE demonstrates the benefit of our approach. Performance modeling
 is deployed to substantiate our performance measurements, and a comparison to
  existing coloring methods as well a vendor optimized library (Intel MKL) 
  are presented. The broad applicability and the sustainability is validated 
  by using a wide set of 31 test matrices and two very different generations 
  of Intel Xeon processors.
\item We extend the existing proven \acrshort{SpMV} performance modeling approach
 to the \acrshort{SymmSpMV} kernel. 
 %In the course of the performance analysis we
 %further demonstrate that SIMD vectorization may negatively impact \acrshort{SymmSpMV}
 %performance. 
In the course of the 
 performance analysis we further demonstrate why in some cases the ideal speedup
 may not be achievable.
%\item  Sustainability of our approach is demonstrated by using two very differenet generation of Intel Xeon processors (IVB and SKX)
%\item --- Christie ---
%	\item Problems with coloring approaches on SymmSpMV
%	\item Performance modeling and analysis
%	\item high alpha pre-factor - preserving data locality more important
%	\item Things to take care on modern CPU's like SKX and caching
%	\item Problems for large matrices and coloring
%	\item RACE method - h/w efficient 
%	\item General approach for D-k coloring
%	\item Study on different corner cases like cache bound, low parallelism, memory bound, mixed effects 
%	\item Why do we not necessarily get ideal 2 $\times$ speedup
%	\item Problems for some special cases like low nnzr
%	\item Comparisons with MKL and coloring approaches
\end{itemize}
We have implemented our graph coloring algorithms in the open source library \acrfull{RACE}.\footnote{\href{http://tiny.cc/RACElib}{http://tiny.cc/RACElib}}
Information required to reproduce the performance numbers provided in this 
paper is also available.\footnote{\href{http://tiny.cc/RACElib-AD}{http://tiny.cc/RACElib-AD}}

%and discuss the impact on a representative set of 31 sparse matrices. Most of the above coloring approaches explained above in \Cref{Sec:related_work} suffer from performance penalties in one way or the other; for example \acrshort{MC}, degrades the data locality, although this can be improved considerably using \acrshort{ABMC}, still for moderately large matrices or with the increase in $k$ of \DK dependency the method shows deterioration in performance. 
%These coloring algorithms and related software frameworks do no take advantage of both load balancing as well as the existing deep memory hierarchy and the characteristics of today's compute nodes to achieve improved node-level performance at full scale.

This paper is organized as follows: Our software and hardware environment as well as
 the benchmark matrices are introduced in \Cref{Sec:test_bed}. 
In \Cref{Sec:test_kernels} we describe the properties
of the \acrshort{SpMV} and \acrshort{SymmSpMV} kernels, including
\roofline performance limits, and motivate the need for an advanced coloring scheme.
In \Cref{Sec:race} we detail the steps of the \acrshort{RACE} algorithm
via an artificial stencil matrix and show how recursive level group construction
and coloring can be leveraged to exploit a desired level of parallelism
for \DK dependencies. The interaction between the parameters of the method
and their impact on the parallel efficiency is studied in \Cref{Sec:param_study}.
\Cref{Sec:expt} presents performance data for \acrshort{SymmSpMV}
for a wide range of matrices on two different multicore systems,
comparing \acrshort{RACE} with \acrshort{ABMC} and \acrshort{MC} as well as
Intel MKL, and also shows the efficiency of \acrshort{RACE} as
defined by the \roofline model yardstick.
\Cref{Sec:conclusion} concludes the paper and gives an outlook to
future work. 


%This paper presents the following contributions to the field of coloring and
%node-level performance engineering. In particular we introduce the
%computational intensity and main memory bandwidth for this kernel operation
%in \Cref{Sec:test_kernels} where we theoretically derive a realistic upper
%performance estimation for a given matrix nonzero pattern.  This performance
%estimation will be later used to evaluate scaling performance and data traffic
%on Intel's Ivy Bridge EP and Skylake SP.

%{\GW ToDo: In \Cref{Sec:race} we present the basic RACE on developing our
%hardware-aware recursive distance-k coloring that can be used as an alternative
%recursive method to parallelize kernels having loop-carried dependencies. By
%introducing an artificially designed stencil we will highlight the benefits of
%the method and how to construct levels, the usage of distance-k coloring, and
%how to obtain load balancing with up to 100 threads. Next we provide a detailed
%performance analysis of the method and comparison against \acrshort{MC}
%and \acrshort{ABMC}. In \Cref{Sec:param_study} we discuss the parameters
%selection and evaluate the performance of \acrshort{RACE} using the roofline
%performance model. In \Cref{Sec:expt} we compare the applicability of our
%recursive coloring method on Intel's Ivy Bridge and Skylake processor systems
%with more traditional approaches such as \acrshort{MC}, \acrshort{ABMC}, and
%also the \acrshort{MKL} implementations. Finally we conclude in
%\Cref{Sec:conclusion}.}


	
\begin{comment}
 \acrshort{RACE}  has two main usage scenarios: It can either return all relevant data structures and parallelization information required for manual implementation of the kernel at hand, or one can just use its callback function interface, which takes care of parallelization and all data handling automatically. As of now \acrshort{RACE} is limited to shared memory nodes as it only supports thread-level parallelism.\acrshort{RACE} uses the \acrfull{CRS} sparse matrix data format but can be easily extended to other formats.
\end{comment}


\begin{comment}
% Coloring and iterative solvers
Coloring techniques have also been extensively used within parallel
Krylov subspace methods. One of the earliest works on parallelizing
kernels within iterative methods having loop-carried dependencies is
the red-black Gauss--Seidel scheme~\cite{RBGS}. Later Kamath and Sameh
introduced a two-block partitioning scheme for parallelizing the
Kaczmarz method on tridiagonal structures~\cite{Kamath}. A first
general study on the convergence of these parallel methods was done
early in 1980 by Elfving~\cite{Elfving1980}. Another line of research
focuses on parallelizing dependent kernels while maintaining the same
convergence behavior of sequential execution. One of the earliest
known works in this category is the hyperplane method~\cite{saad} on
FDM (finite difference method) like matrices. Extensions to this
approach is given in ~\cite{cm-rcm} where a hybrid approach
between \acrshort{MC} and the hyperplane method is used. However, the
most general method which falls into this category is
level-scheduling{\CA should I remove hyphen?}~\cite{saad}.  Efficient
implementation of this method on triangular solvers can be attributed
to Park \etal ~\cite{park_ls}. Most of the above mentioned methods
have been tested only for their applicability to parallelize \DONE
dependent kernels and some of them are not capable of dealing with
dependencies like \DTWO. The research on parallelizing \DONE dependent
kernels has been strongly accelerated after the introduction of the
HPCG benchmark~\cite{hpcg}. When it comes to \DTWO kernels, popular
methods seen in the literature are locking-based methods, thread
private local vectors~\cite{sparseX,thread_private_symm_spmv} for
kernels like symmetric sparse matrix vector {\CA should it be
vectors?} or with the usage of specially tailored sparse matrix data
formats like compressed sparse blocks (CSB)~\cite{CSB}
or \acrfull{RSB}~\cite{RSB}.
\end{comment}



\begin{comment}
As a final application run we demonstrate the parallelization of an eigen-value solver called FEAST \cite{FEAST}, where we use an iterative inner linear solver based on Kaczmarz method. The result presented is the first to achieve such high performance on node level for an iterative solver and is superior to the previous results published \cite{feast_mc}.
\end{comment}
