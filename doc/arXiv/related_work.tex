The efficient solution of linear systems or eigenvalue problems
involving large sparse matrices has been an active research field in
parallel and high performance computing for many decades. Well-known,
traditional application areas include quantum physics, quantum
chemistry or engineering. In recent years, new fields such as social
graph analysis ~\cite{Simpson:2018:BGP:3218176.3218232} or spectral
clustering in the context of learning
algorithms \cite{vonLuxburg2007,JMLR:v17:16-109} have further
increased the need for hardware-efficient, parallel sparse solvers
and/or efficient matrix-free solvers. Assuming sufficiently large
problems, the solvers are typically based on iterative subspace
methods and may include advanced preconditioning techniques. In many
methods, two components, \acrfull{SpMV} and coloring techniques, are
crucial for hardware efficiency and parallel scalability. Typically,
these two components are considered to be orthogonal, \ie hardware
efficiency for \Acrshort{SpMV} is mainly related to data formats and
local structures while coloring is used to address dependencies in the
enclosing iteration scheme.  Interestingly, the hardware-efficient
parallelization of symmetric \Acrshort{SpMV} has not attracted a lot
of attention over the years, though symmetry is widespread in the
application fields.

The \Acrshort{SpMV} operation is an essential building block 
in a number of applications such as algebraic multigrid methods, 
sparse iterative solvers, shortest path algorithms, breadth first search algorithms, 
and Markov cluster algorithms, and therefore it is an integral part 
of numerous scientific algorithms.
In the past decades, much research has been
focusing on designing new data structures, efficient algorithms, and
parallelization  techniques for the \acrshort{SpMV} operation. Its performance is typically limited by main memory bandwidth. On
cache-based architectures, the main factors that influence performance
are spatial access locality to the matrix data and temporal locality when
reusing the elements of the vectors involved. To address this problem, over the
last two decades a plethora of  partitioning techniques
and data structures to improve \acrshort{SpMV} on cache-based
architectures have been suggested, including %greedy graph coloring techniques, 
cache-oblivious methods using hypergraph partitioning. One
of the first studies on temporal locality optimizations was done
by Toledo~\cite{Toledo:1997:IMP:279511.279532}, who investigated
Cuthill--McKee (CM) ordering
techniques on three-dimensional finite-element test matrices when
used in combination with blocking into small dense blocks. Various
authors~\cite{Williams:2009:OSM:1513001.1513318,doi:10.1177/1094342004041296}
used advanced data storage formats and techniques such as register and cache blocking  for  
\acrshort{SpMV} by splitting the matrix into several smaller $p \times
q$ sparse submatrices and presented an analytic cache-aware model to
determine the optimal block size. These algorithms are, e.g.,
included in OSKI~\cite{1742-6596-16-1-071}, which is a collection of
low-level primitives of tuned sparse kernels for modern cache-based
superscalar machines. Kreutzer \etal~\cite{Moritz_sell} and 
Xing \etal~\cite{Liu:2013:ESM:2464996.2465013} used techniques to 
improve SIMD efficiency and performance on many-core and GPU architectures. 
Recent work can be found, \eg
in~\cite{li2017hbm,Liu:2015:CES:2751205.2751209,liu2015spmv}.
 Previous work on  \acrshort{SpMV}  has also focused on reducing
 communication volume for distributed-memory parallelization, often by using
 variants of graph or hypergraph partitioning
 techniques~\cite{Catalyurek:1999}. Yzelman and
 Bisseling~\cite{doi:10.1137/080733243,Yzelman-thesis-2011} extended
 hypergraph partitioning techniques in a cache-oblivious method,
 permuting rows and columns of the input matrix using a recursive
 hypergraph-based sparse matrix partitioning scheme so that the
 resulting matrix exhibits cache-friendly behavior during the
 \acrshort{SpMV}.

%\begin{itemize}
%	\item More linking from \acrshort{SpMV} to \acrshort{SymmSpMV}
%	\item Many have concentrated on general \acrshort{SpMV}, with methods exploiting
%	 structure in matrix like blocking, tuning and using tailored data storage formats and so on.
%	 \item Performance modeling of general \acrshort{SpMV} is extensively carried out,
%	 but there is no such work on \acrshort{SymmSpMV}.
%\end{itemize}
%More recently Cheshmi~\etal~\cite{Cheshmi:2018:PIT:3291656.3291739} used e.g. a
%novel task coarsening strategy to create well-balanced tasks that can
%execute in parallel, while maintaining locality of memory accesses. 


%\cite{Buluc:2011:RMA:2058524.2059503

Despite \acrshort{SpMV} being a bandwidth-limited operation, not much work has 
been done to exploit the symmetry property of symmetric matrices to reduce
storage requirements and data transfers by using only the upper/lower triangular part of the matrix.
% and hence  optimizing storage and data transfer of symmetric matrices .
The major challenge here is to resolve the potential write conflicts of explicit 
\acrfull{SymmSpMV} kernels in parallel processing.
%is mainly due to the dependency problems that arise with the parallelization of the  \acrshort{SymmSpMV} operation. 
There are general solutions for such problems like 
lock based methods and thread private target
arrays~\cite{sparseX,thread_private_symm_spmv,Krotkiewski:2010:PSS:1752612.1752682,Mironowicz:2015}. However they have in common that their overhead may increase with the degree of parallelism.
%which may potentially increase overhead with the level of parallelism.
Another recent research direction is the use of specialized storage formats 
like CSB \cite{CSB}, RSB \cite{RSB}, CSX \cite{sparseX} combined with the use of bitmasked 
register blocking techniques as in \cite{Buluc:2011:RMA:2058524.2059503}. As pointed out 
by \cite{liu2015spmv} these approaches have drawbacks like missing backward compatibility and matrix conversion costs. 
Due to these problems there are only a very few standard libraries, like 
\acrshort{MKL}~\cite{MKL}, that support primitives for efficient \acrshort{SymmSpMV} operation.
Another potential way of tackling this inherent data dependency problem is using a \DTWO coloring 
 of the underlying undirected graph, which has not been investigated so far to the best of our knowledge.

%\begin{itemize} % Das ist was fuer COnclusion and Outlook
%	\item Another point in favor of coloring is most of the above approaches would work only for \acrshort{SymmSpMV} kernels and are not suitable for D-2 solvers like KACZ.
%\end{itemize}

\Acrfull{MC} reordering to tackle data dependencies is a very well established strategy in parallelization of iterative solvers. As it is applied to the underlying graph it is not bound to a specific data format and may use existing highly optimized (serial) kernels, \ie it is orthogonal to general code optimization strategies.  
Prominent examples for  \acrshort{MC} in iterative solvers are Gauss-Seidel, incomplete 
Cholesky factorization or Kaczmarz method~\cite{RBGS,MC,feast_mc}, where typically a \DONE or \DTWO coloring is applied subject to the underlying dependencies of the iterative scheme. However, coloring changes the evaluation order of the original solver and may lead to worse convergence rates. This is different when using  \acrshort{MC} methods for parallelization of \acrshort{SymmSpMV} where we only need to ensure that entries of the target vector is not written in parallel. Here we do not  require strict serial ordering to get to the same result as in serial processing.
In terms of hardware utilization long-standing \acrshort{MC} methods often generate colorings which lack efficiency on modern cache-based
processors. Studies have been made to increase their
performance and improve inherent heuristics; an overview of the methods can be found in~\cite{gebremedhin2000scalable,dist_k_def,COLPACK,equitable_color}. However, 
for irregular and/or large sparse 
matrices \acrshort{MC} may lead to load imbalance, frequent global synchronization, 
and loss of data locality, severely reducing (single-node) performance. 
These problems typically become more stringent for higher order distance
colorings and larger matrices.
The \acrfull{ABMC}~\cite{ABMC} proposed by Iwashita \etal in 2012 addresses some of these issues as it tries to increase data locality by applying graph partitioning (blocking) before coloring. 
Beyond the quality of the actual coloring, the time to generate it is also critical, especially for very large problems. Here, widely used and publicly available coloring packages such as COLPACK\cite{COLPACK}, Kokkos\cite{kokkos} and ZOLTAN\cite{BOZDAG2008515,doi:10.1137/080732158} speed-up the coloring process itself by parallelization and other heuristics. 

\begin{comment}
{\GW Olaf please check: Beziehen sich diese beiden Saetze un Referenzen auf den COloring Prozess? Falls ja - benoetigen wir diese? Dazu sagen wir ja gar nichts}
In many applications it is important to compute a coloring with few
colors in near-linear time~\cite{doi:10.1137/13093426X}.
In parallel coloring methods, the optimistic (speculative) coloring method by Gebremedhin
and Manne~\cite{gebremedhin2000scalable} is the preferred
approach~\cite{Boman:2016}.  
{\GW Olaf please check: Der folgende Satz steht meiner Meinung nach im Widerspruch zu unserer Aussage, dass noch niemand Coloring fuer Parallelisierung von SymmSpMV gemacht hat - siehe oben. Oder hast du da andere Informationen.}
Until recently, only a few of these
coloring and partitioning technique concepts made it into mainstream
software, but the increasingly stringent performance requirements for
fast \acrshort{SymmSpMV} multiplication have resulted in a number of recent
implementations that have adopted these concepts, namely,
{\GW Olaf please check: Meinst du dass MKL und KOKKOS primitives fuers Coloring oder fuer SymSpMV haben? Lt Christie hat MKL SymmSpMV Routinen aber KOKKOS nicht und beim Coloring ist es genau anders rum.}
the \acrshort{MKL} library~\cite{MKL} and node-level programming and
performance primitives from the Trilinos' Kokkos node
package~\cite{kokkos}. 
{\GW Olaf please check: Ist es OK wenn wir die folgenden Saetze mit dem letzten Satz des vorherigen Abschnittes abhandeln?}
A distributed memory coloring framework for
distance-1~\cite{BOZDAG2008515} and
distance-2~\cite{doi:10.1137/080732158} coloring have been implemented
in Zoltan. The framework provides efficient implementation for greedy
graph coloring algorithms and it provides parallel dynamic load
balancing and related services for a wide variety of applications,
including finite-element methods and matrix operations.  
\end{comment}

%
%{\GW Olaf please check: Das wuerde ich allgemeiner formulieren: Tebdenz: Mehr Threads; hoehere Bandbreite aber auch uU kleinere Memories (HBM etc) --> explizite hw-effiziente SymmSPMV kernels inreasingly important.}
%However, it has been very clear, for some time, that in order to provide better
%support of \acrshort{SymmSpMV} running on the next generation of
%computing platforms, fundamental problems related to model
%representation and coloring techniques on modern hardware accelerated
%computational platforms has to be revisted again. One needs to think
%deeper about the ramifications to the entire software stack so
%that one can transition the algorithms to once again realign with the
%realities of the underlying hardware constraints. 

%\CAcomm{Currently I have not included Cheshmi~\etal~\cite{Cheshmi:2018:PIT:3291656.3291739}  as it 
%	does not fit to the text.}
	
Design and implementation of hardware efficient computational kernels can be
supported by a structured performance engineering process based on white-box
models. On the processor/node level, the most prominent model is the roof{}line
model~\cite{Williams_roofline}. Its basic applicability as a reasonable
light-speed estimate for \acrshort{SpMV} was  demonstrated already 
in~\cite{Gropp:1999}, including an extension to sparse matrix multiple vector
multiplication. The \acrshort{SpMV} performance model was refined
in~\cite{Moritz_sell} with a focus on modeling the performance impact
of irregular accesses to the right-hand side (RHS) vector. It has been
successfully used to model performance on CPUs and GPGPUs for \acrshort{SpMV}
kernels~\cite{Moritz_sell} and for augmented sparse matrix multiple vector kernels
for Chebyshev filter diagonalization~\cite{ISC2018:ChebFD}. However, there is no
extension towards explicit \acrshort{SymmSpMV}, which shows increased
computational intensity and irregular accesses to both involved vectors. Typically the
expectation is that \acrshort{SymmSpMV} should be approximately twice as fast
as \acrshort{SpMV} as only half of the matrix information needs to be stored and
accessed.

Finally, there is a clear hardware trend towards processors with advanced
vector-style processing, higher core counts and more complex cache
hierarchies. Also, attainable bandwidth may increase even for ``standard'' CPU
based systems through the use of high bandwidth memory solutions at the cost of
very restricted memory sizes. A first step into this direction was the Intel
Xeon Knights Landing processor. The specification of the ARM-based Fujitsu
A64FX processor (to be used in the Post-K computer) may provide another
blueprint for future processor configurations~\cite{Post-K:Processor}: A 48-core
processor supporting 512-bit SIMD execution units on top of 32 \GiB HBM2 main
memory, which provides a bandwidth of 1 \TB/s.  It is obvious that such hardware
trends call for revisiting existing, time-critical components in simulation
codes both in terms of scalability and hardware efficiency. Moreover, the
potential of \acrshort{SymmSpMV} to substantially reduce the memory footprint of
sparse solvers needs to be exploited to meet the constraint of very limited
memory space.

